<?php
$title = "Vélos";
require_once './assets/php/header.php';
require_once './assets/php/connection.php';
require_once './assets/php/utils.php';

// variables
$is_add_view = false;
$edit_id = false;
$edit_values = [];
$insertion_success = "";
$error_popup = "";
$ls_marques = "";
$ls_stations = "";
$consultation = false;
$bikes_by_station_result = [];
$bikes_being_used_result = [];
$bikes_by_station = "Vélos par Station";
$bikes_being_used = "Vélos en cours d'utilisation";
$nb_bike = 0;

// differentes operations
if ($_SERVER["REQUEST_METHOD"] === "POST") {
    switch ($_POST["formname"]) {
    case "delete":
        $r = pg_query_params($conn, 'DELETE FROM velos WHERE id_velo=$1;', array($_POST["id"]));
        if (!$r) $error_popup = generate_message(pg_last_error());
        else pg_free_result($r);
        break;
    case "pre_add":
        $is_add_view = true;
        break;
    case "add":
        $consultation = false;
        $r = pg_query_params($conn, "
            INSERT INTO velos(reference, date_mise_en_service, etat, niveau_charge_batterie, id_marque, id_station)
            VALUES ($1, $2, $3, $4, $5, $6) RETURNING CURRVAL('velos_id_velo_seq');",
            array($_POST["reference"], $_POST["date_mise_en_service"],
                    $_POST["etat"], $_POST["niveau_charge_batterie"],
                    $_POST["marque"], $_POST["station"] === "null" ? null : $_POST["station"])
        );
        if (!$r) $error_popup = generate_message(pg_last_error());
        else {
            $insertion_success = generate_message("Le vélo a bien été ajouté avec succès, avec l'identifiant " . pg_fetch_row($r)[0], "Succès !", "success");
            pg_free_result($r);
        }
        break;
    case "pre_edit":
        $edit_id = $_POST["id"];
        $r = pg_query_params($conn, "SELECT * FROM velos WHERE id_velo = $1;", array($edit_id));
        if (!$r) $error_popup = generate_message(pg_last_error());
        else {
            $edit_values = pg_fetch_assoc($r);
            if (!$edit_values) $error_popup = generate_message(pg_last_error());
            pg_free_result($r);
        }
        break;
    case "edit":
        $consultation = false;
        $r = pg_query_params($conn, "
            UPDATE velos SET reference = $1, date_mise_en_service = $2, etat = $3,
            niveau_charge_batterie = $4, id_marque = $5, id_station = $6 WHERE id_velo = $7;",
            array($_POST["reference"], $_POST["date_mise_en_service"],
                    $_POST["etat"], $_POST["niveau_charge_batterie"],
                    $_POST["marque"], $_POST["station"] === "null" ? null : $_POST["station"], $_POST["edit_id"])
        );
        if (!$r) $error_popup = generate_message(pg_last_error());
        else {
            $insertion_success = generate_message("La mise à jour s'est bien déroulée pour le vélo dont l'identifiant est " . $_POST["edit_id"], "Succès !", "success");
            pg_free_result($r);
        }
        break;
    case $bikes_by_station:
        $consultation = true;
        $bikes_by_station_result = pg_query($conn, "SELECT COALESCE(adresse_station, '- Non disponible') AS adresse_station, 
                                                COMMUNES.nom_ville, VELOS.id_velo, VELOS.reference, 
                                                VELOS.date_mise_en_service, VELOS.etat, 
                                                VELOS.niveau_charge_batterie, MARQUES.nom_marque
                                                FROM VELOS
                                                NATURAL JOIN MARQUES
                                                NATURAL LEFT JOIN STATIONS
                                                NATURAL LEFT JOIN COMMUNES
                                                ORDER BY STATIONS.id_station, STATIONS.adresse_station, VELOS.reference;");
        if (!$bikes_by_station_result) $error_popup = generate_message(pg_last_error());
        break;
    case $bikes_being_used:
        $consultation = true;
        $bikes_being_used_result = pg_query($conn, "SELECT VELOS.id_velo, VELOS.reference, VELOS.date_mise_en_service, 
                                                VELOS.etat, VELOS.niveau_charge_batterie, MARQUES.nom_marque
                                                FROM VELOS
                                                NATURAL JOIN MARQUES
                                                WHERE id_station IS NULL;");
        if (!$bikes_being_used_result) $error_popup = generate_message(pg_last_error());
        break;
    default:
        break;
    }
}

// si on ajoute ou modifie l'entite et qu'il y a des cles etrangeres, recuperer les valeurs
if ($is_add_view || $edit_id) {
    // recherche marques
    $r = pg_query($conn, "SELECT id_marque, nom_marque FROM marques;");
    if (!$r) $error_popup = generate_message(pg_last_error());
    else {
        $ls_marques = "";
        while ($l = pg_fetch_row($r))
            $ls_marques .= "<option value=\"" . $l[0] . "\"" . ($edit_id && $edit_values["id_marque"] === $l[0] ? "selected" : "") . ">" . $l[1] . "</option>";
        pg_free_result($r);
    }

    // recherche stations
    $r = pg_query($conn, "SELECT id_station, adresse_station, nom_ville FROM stations NATURAL JOIN communes;");
    if (!$r) $error_popup = generate_message(pg_last_error());
    else {
        $ls_stations = "";
        while ($l = pg_fetch_row($r))
            $ls_stations .= "<option value=\"" . $l[0] . "\"" . ($edit_id && $edit_values["id_station"] === $l[0] ? "selected" : "") . ">" . $l[1] . " (" . $l[2] . ")</option>";
        $ls_stations .= "<option value=\"null\"" . ($edit_id && $edit_values["id_station"] === NULL ? "selected" : "") . "> - Non disponible - </option>";
        pg_free_result($r);
    }
} else { // sinon si c'est la vue normale, afficher les colonnes/lignes
    $result = pg_query($conn, "SELECT COUNT(*) FROM velos");
    if (!$result) $error_popup = generate_message(pg_last_error());
    else {
        if (!($r = pg_fetch_row($result))) $error_popup = generate_message(pg_last_error());
        $nb_bike = $r[0];

        pg_free_result($result);

        $query = "SELECT id_velo, reference, date_mise_en_service, etat, niveau_charge_batterie,
        COALESCE(adresse_station, '- Non disponible') AS adresse_station,
        comm.nom_ville AS nom_ville, nom_marque
        FROM velos
        NATURAL JOIN marques
        NATURAL LEFT JOIN stations
        LEFT JOIN communes AS comm
        ON (stations.id_commune=comm.id_commune)
        ORDER BY id_velo;";

        if ($bikes_by_station_result) $result = $bikes_by_station_result;
        else if ($bikes_being_used_result) $result = $bikes_being_used_result;
        else $result = pg_query($conn, $query);

        if (!$result) $error_popup = generate_message(pg_last_error());
        else {
            $tbody = "";

            while ($r = pg_fetch_assoc($result)) {
                $tbody .= "<tr>";
                $tbody .= "<td>" . $r["id_velo"] . "</td>";
                $tbody .= "<td>" . $r["reference"] . "</td>";
                $tbody .= "<td>" . $r["date_mise_en_service"] . "</td>";
                $tbody .= "<td>" . $r["etat"] . "</td>";
                $tbody .= "<td>" . $r["niveau_charge_batterie"] . " %</td>";
                if (!$bikes_being_used_result)
                    $tbody .= "<td>" . $r["adresse_station"] . " - " . $r["nom_ville"] . "</td>";
                $tbody .= "<td>" . $r["nom_marque"] . "</td>";
                if (!$consultation) {
                    $tbody .= "<td>" . createEditButton($r["id_velo"]) . "</td>";
                    $tbody .= "<td>" . createDeleteButton($r["id_velo"]) . "</td>";
                }
                $tbody .= "</tr>\n";
            }

            pg_free_result($result);
        }
    }
}

pg_close($conn);
?>
<div class="container">
    <h1>Vélos</h1>
    <!-- messages -->
    <?php echo empty($error_popup) ? $insertion_success : $error_popup; ?>
    <?php if ($is_add_view || $edit_id ) { ?>
    <!-- ajouter le formulaire d'ajout/de modification si on le demande -->
    <h2><?php echo $is_add_view ? "Ajouter un vélo dans la base de données." : "Éditer un vélo."; ?></h2>
    <form action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="POST">
        <?php if ($edit_id) { ?>
        <input type="hidden" name="edit_id" value="<?php echo $edit_id; ?>">
        <?php } ?>
        <input type="hidden" name="formname" value="<?php echo ($edit_id ? "edit" : "add"); ?>">
        <!-- reference -->
        <label for="reference" class="form-label">Référence</label>
        <input type="text" name="reference" placeholder="Vélo #001" class="form-control mb-3" required value="<?php echo ($edit_id ? $edit_values["reference"] : ""); ?>">
        <!-- date mise en service -->
        <label for="date_mise_en_service" class="form-label">Date de mise en service</label>
        <input type="date" name="date_mise_en_service" class="form-control mb-3" required value="<?php echo ($edit_id ? $edit_values["date_mise_en_service"] : ""); ?>">
        <!-- etat -->
        <label for="etat" class="form-label">État</label>
        <input type="text" class="form-control mb-3" name="etat" placeholder="Etat" required value="<?php echo ($edit_id ? $edit_values["etat"] : ""); ?>">
        <!-- niveau de charge -->
        <label for="niveau_charge_batterie" class="form-label">Niveau de charge de la batterie</label>
        <input type="number" min="0" max="100" class="form-control mb-3" name="niveau_charge_batterie" placeholder="50" required value="<?php echo ($edit_id ? $edit_values["niveau_charge_batterie"] : ""); ?>">
        <!-- marque -->
        <label for="marque" class="form-label">Marque</label>
        <select name="marque" class="form-control mb-3" required value="<?php echo ($edit_id ? $edit_values["id_marque"] : ""); ?>"><?php echo $ls_marques;?></select>
        <!-- station -->
        <label for="station" class="form-label">Station</label>
        <select name="station" class="form-control mb-3" required><?php echo $ls_stations; ?></select>
        <button type="submit" class="btn btn-<?php echo ($edit_id ? "warning" : "primary"); ?> mb-3"><?php echo ($edit_id ? "Éditer" : "Ajouter"); ?></button>
    </form>
    <?php } else { ?>
    <!-- sinon, les colonnes/lignes -->
    <p>Il y a actuellement <b><?php echo $nb_bike; ?></b> <?php echo $nb_bike === "1" ? "vélo" : "vélos"; ?> dans la base de données.</p>
    <div class="row row-cols-auto">
        <div class="col"><?php echo createAddButton(); ?></div>
        <div class="col"><?php echo createConsultationButton($bikes_by_station); ?></div>
        <div class="col"><?php echo createConsultationButton($bikes_being_used); ?></div>
    </div>
    <?php 
    if ($bikes_by_station_result)
        echo "<h2>Voici les vélos par station :</h2>";
    else if ($bikes_being_used_result)
        echo "<h2>Les vélos en cours d'utilisation sont :</h2>";
    else
        echo "<h2>Les vélos dans la base de données sont :</h2>";
    ?>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>ID</th>
                <th>Référence</th>
                <th>Date de mise en service</th>
                <th>État</th>
                <th>Niveau de charge de la batterie</th>
                <?php
                if (!$bikes_being_used_result)
                    echo "<th>Station</th>";
                ?>
                <th>Marque</th>
                <?php
                if (!$consultation)
                    echo "<th>Éditer</th><th>Supprimer</th>";
                ?>
            </tr>
        </thead>
        <tbody>
            <?php echo $tbody; ?>
        </tbody>
    </table>
    <?php } ?>
</div>
