<?php
$title = "Communes";
require_once './assets/php/header.php';
require_once './assets/php/connection.php';
require_once './assets/php/utils.php';

// variables
$is_add_view = false;
$edit_id = false;
$edit_values = [];
$insertion_success = "";
$error_popup = "";
$nb_city = 0;

// differentes operations
if ($_SERVER["REQUEST_METHOD"] === "POST") {
    switch ($_POST["formname"]) {
    case "delete":
        $r = pg_query_params($conn, 'DELETE FROM communes WHERE id_commune=$1;', array($_POST["id"]));
        if (!$r) $error_popup = generate_message(pg_last_error());
        else pg_free_result($r);
        break;
    case "pre_add":
        $is_add_view = true;
        break;
    case "add":
        $r = pg_query_params($conn, "
            INSERT INTO communes(nom_ville)
            VALUES ($1) RETURNING CURRVAL('communes_id_commune_seq');",
            array($_POST["nom_ville"])
        );
        if (!$r) $error_popup = generate_message(pg_last_error());
        else {
            $insertion_success = generate_message("La communne a bien été ajouté avec succès, avec l'identifiant " . pg_fetch_row($r)[0], "Succès !", "success");
            pg_free_result($r);
        }
        break;
    case "pre_edit":
        $edit_id = $_POST["id"];
        $r = pg_query_params($conn, "SELECT * FROM communes WHERE id_commune = $1;", array($edit_id));
        if (!$r) $error_popup = generate_message(pg_last_error());
        else {
            $edit_values = pg_fetch_assoc($r);
            if (!$edit_values) $error_popup = generate_message(pg_last_error());
            pg_free_result($r);
        }
        break;
    case "edit":
        $r = pg_query_params($conn, "
        UPDATE communes SET nom_ville = $1 WHERE id_commune = $2;",
        array($_POST["nom_ville"], $_POST["edit_id"])
        );
        if (!$r) $error_popup = generate_message(pg_last_error());
        else {
            $insertion_success = generate_message("La mise à jour s'est bien déroulée pour la commune dont l'identifiant est " . $_POST["edit_id"], "Succès !", "success");
            pg_free_result($r);
        }
        break;
    default:
        break;
    }
}

// si c'est la vue normale, afficher les colonnes/lignes (pas de cles etrangeres ici)
if (!$is_add_view && !$edit_id) {
    $result = pg_query($conn, "SELECT COUNT(*) FROM communes");
    if (!$result) $error_popup = generate_message(pg_last_error());
    else {
        if (!($r = pg_fetch_row($result))) $error_popup = generate_message(pg_last_error());
        $nb_city = $r[0];

        pg_free_result($result);

        $result = pg_query($conn, "SELECT * FROM communes ORDER BY id_commune");
        if (!$result) $error_popup = generate_message(pg_last_error());
        else {
            $tbody = "";
            
            while ($r = pg_fetch_assoc($result)) {
                $tbody .= "<tr>";
                $tbody .= "<td>" . $r["id_commune"] . "</td>";
                $tbody .= "<td>" . $r["nom_ville"] . "</td>";
                $tbody .= "<td>" . createEditButton($r["id_commune"]) . "</td>";
                $tbody .= "<td>" . createDeleteButton($r["id_commune"]) . "</td>";
                $tbody .= "</tr>\n";
            }
            pg_free_result($result);
        }
    }
}
pg_close($conn);
?>
<div class="container">
    <h1>Communes</h1>
    <!-- messages -->
    <?php echo empty($error_popup) ? $insertion_success : $error_popup; ?>
    <!-- ajouter le formulaire d'ajout/de modification si on le demande -->
    <?php if ($is_add_view || $edit_id ) { ?>
    <h2><?php echo $is_add_view ? "Ajouter une commune dans la base de données." : "Éditer une commune."; ?></h2>
    <form action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="POST">
        <?php if ($edit_id) { ?>
        <input type="hidden" name="edit_id" value="<?php echo $edit_id; ?>">
        <?php } ?>
        <input type="hidden" name="formname" value="<?php echo ($edit_id ? "edit" : "add"); ?>">
        <!-- nom ville -->
        <label for="nom_ville" class="form-label">Référence</label>
        <input type="text" name="nom_ville" placeholder="Commune #001" class="form-control mb-3" required value="<?php echo ($edit_id ? $edit_values["nom__de_ville"] : ""); ?>">
        <button type="submit" class="btn btn-<?php echo ($edit_id ? "warning" : "primary"); ?> mb-3"><?php echo ($edit_id ? "Éditer" : "Ajouter"); ?></button>
    </form>
    <?php } else { ?>
    <p>Il y a actuellement <b><?php echo $nb_city; ?></b> <?php echo $nb_city === "1" ? "commune" : "communes"; ?> dans la base de données.</p>
    <?php echo createAddButton(); ?>
    <h2>Les communes dans la base de données sont :</h2>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>ID</th>
                <th>Nom de ville</th>
                <th>Éditer</th>
                <th>Supprimer</th>
            </tr>
        </thead>
        <tbody>
            <?php echo $tbody; ?>
        </tbody>
    </table>
    <?php } ?>
</div>
