<?php
$title = "Distances entre stations";
require_once './assets/php/header.php';
require_once './assets/php/connection.php';
require_once './assets/php/utils.php';

// variables
$is_add_view = false;
$insertion_success = "";
$error_popup = "";
$nb_distance = 0;

// differentes operations
if ($_SERVER["REQUEST_METHOD"] === "POST") {
    switch ($_POST["formname"]) {
    case "pre_add":
        $is_add_view = true;
        break;
    case "add":
        $r = pg_query_params($conn, "
            INSERT INTO etre_distante_de (distance, id_station_1, id_station_2)
            VALUES ($1, $2, $3);",
            array($_POST["distance"], $_POST["station_1"], $_POST["station_2"])
        );
        if (!$r) $error_popup = generate_message(pg_last_error());
        else {
            $insertion_success = generate_message("La distance a bien été ajouté avec succès" . pg_fetch_row($r)[0], "Succès !", "success");
            pg_free_result($r);
        }
        break;
    default:
        break;
    }
}

// si on ajoute l'entite et qu'il y a des cles etrangeres, recuperer les valeurs
if ($is_add_view) {
    // recherche stations
    $r = pg_query($conn, "SELECT id_station, adresse_station, nom_ville FROM stations NATURAL JOIN communes;");
    if (!$r) $error_popup = generate_message(pg_last_error());
    else {
        $ls_stations = "";
        while ($l = pg_fetch_row($r))
            $ls_stations .= "<option value=\"" . $l[0] . "\" > " . $l[1] . " (" . $l[2] . ")</option>";
        pg_free_result($r);
    }
} else {
    $result = pg_query($conn, "SELECT COUNT(*) FROM etre_distante_de");
    if (!$result) $error_popup = generate_message(pg_last_error());
    else {
        if (!($r = pg_fetch_row($result))) $error_popup = generate_message(pg_last_error());
        $nb_distance = $r[0];
        
        pg_free_result($result);
        
        $query = "SELECT distance, st1.adresse_station AS ad1, st2.adresse_station AS ad2, c1.nom_ville AS cst1, c2.nom_ville AS cst2
                FROM etre_distante_de 
                INNER JOIN stations AS st1 
                ON (etre_distante_de.id_station_1 = st1.id_station) 
                INNER JOIN stations AS st2 
                ON (etre_distante_de.id_station_2 = st2.id_station)
                INNER JOIN communes AS c1
                ON (st1.id_commune=c1.id_commune)
                INNER JOIN communes AS c2
                ON (st2.id_commune=c2.id_commune)";
        $result = pg_query($conn, $query);
        if (!$result) $error_popup = generate_message(pg_last_error());
        else {
            $tbody = "";
            
            while ($r = pg_fetch_assoc($result)) {
                $tbody .= "<tr>";
                $tbody .= "<td>" . $r["ad1"] . " - " . $r["cst1"] . "</td>";
                $tbody .= "<td>" . $r["ad2"] . " - " . $r["cst2"] . "</td>";
                $tbody .= "<td>" . $r["distance"] . "m</td>";
                $tbody .= "</tr>\n";
            }
        
            pg_free_result($result);
        }
    }
}
pg_close($conn);
?>
<div class="container">
    <h1>Distances entre stations</h1>
    <!-- messages -->
    <?php echo empty($error_popup) ? $insertion_success : $error_popup; ?>
    <?php if ($is_add_view) { ?>
    <h2><?php echo "Ajouter une distance dans la base de données."; ?></h2>
    <form action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="POST">
        <input type="hidden" name="formname" value="<?php echo ("add"); ?>">
        <label for="distance" class="form-label">Distance</label>
        <input type="number" min="0" class="form-control mb-3" name="distance" required>
        <label for="station_1" class="form-label">Station 1</label>
        <select name="station_1" class="form-control mb-3" required><?php echo $ls_stations;?></select>
        <label for="station_2" class="form-label">Station 2</label>
        <select name="station_2" class="form-control mb-3" required><?php echo $ls_stations;?></select>
        <button type="submit" class="btn btn-<?php echo ("primary"); ?> mb-3"><?php echo ("Ajouter"); ?></button>
    </form>
    <?php } else { ?>
    <p>Il y a actuellement <b><?php echo $nb_distance; ?></b> <?php echo $nb_distance === "1" ? "distance" : "distances"; ?> entre stations dans la base de données.</p>
    <?php echo createAddButton(); ?>
    <h2>Les distances entre stations dans la base de données sont :</h2>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Stations 1</th>
                <th>Station 2</th>
                <th>Distance</th>
            </tr>
        </thead>
        <tbody>
            <?php echo $tbody; ?>
        </tbody>
    </table>
    <?php } ?>
</div>
