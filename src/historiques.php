<?php
$title = "Historique des utilisations";
require_once './assets/php/header.php';
require_once './assets/php/connection.php';
require_once './assets/php/utils.php';

// variables
$is_add_view = false;
$edit_id = false;
$edit_values = [];
$insertion_success = "";
$error_popup = "";
$ls_velos = "";
$ls_adherents = "";
$ls_station_arrivees = "";
$ls_station_departs = "";
$nb_history = 0;

// differentes operations
if ($_SERVER["REQUEST_METHOD"] === "POST") {
    switch ($_POST["formname"]) {
    case "delete":
        $r = pg_query_params($conn, 'DELETE FROM historique_utilisations WHERE id_utilisation=$1;', array($_POST["id"]));
        if (!$r) $error_popup = generate_message(pg_last_error());
        else pg_free_result($r);
        break;
        case "pre_add":
            $is_add_view = true;
            break;
        case "add":
                $r = pg_query_params($conn, "
                    INSERT INTO historique_utilisations(date_debut_utilisation, date_fin_utilisation, id_velo, id_adherent, id_station_depart, id_station_arrivee)
                    VALUES ($1, $2, $3, $4, $5, $6) RETURNING CURRVAL('historique_utilisations_id_utilisation_seq');",
                    array($_POST["date_debut_utilisation"], empty($_POST["date_fin_utilisation"]) ? null : $_POST["date_fin_utilisation"], $_POST["velo"],
                            $_POST["adherent"], $_POST["station_depart"],
                            $_POST["station_arrivee"] === "null" ? null : $_POST["station_arrivee"])
                );
                if (!$r) $error_popup = generate_message(pg_last_error());
                else {
                    $insertion_success = generate_message("L'historique a bien été ajouté avec succès, avec l'identifiant " . pg_fetch_row($r)[0], "Succès !", "success");
                    pg_free_result($r);
                }
            break;
        case "pre_edit":
            $edit_id = $_POST["id"];
            $r = pg_query_params($conn, "SELECT * FROM historique_utilisations WHERE id_utilisation = $1;", array($edit_id));
            if (!$r) $error_popup = generate_message(pg_last_error());
            else {
                $edit_values = pg_fetch_assoc($r);
                if (!$edit_values) $error_popup = generate_message(pg_last_error());
                pg_free_result($r);
            }
            break;
        case "edit":
                $r = pg_query_params($conn, "
                    UPDATE historique_utilisations SET date_debut_utilisation = $1, date_fin_utilisation = $2, id_velo = $3, id_adherent = $4,
                    id_station_depart = $5, id_station_arrivee = $6 WHERE id_utilisation = $7;",
                    array($_POST["date_debut_utilisation"], empty($_POST["date_fin_utilisation"]) ? null : $_POST["date_fin_utilisation"], $_POST["velo"],
                            $_POST["adherent"], $_POST["station_depart"],
                            $_POST["station_arrivee"] === "null" ? null : $_POST["station_arrivee"], $_POST["edit_id"])
                );
                if (!$r) $error_popup = generate_message(pg_last_error());
                else {
                    $insertion_success = generate_message("La mise à jour s'est bien déroulée pour l'historique dont l'identifiant est " . $_POST["edit_id"], "Succès !", "success");
                    pg_free_result($r);
                }
            break;
    default:
        break;
    }
}

// si on ajoute ou modifie l'entite et qu'il y a des cles etrangeres, recuperer les valeurs
if ($is_add_view || $edit_id) {
    // recherche velos
    $r = pg_query($conn, "SELECT id_velo, reference FROM velos;");
    if (!$r) $error_popup = generate_message(pg_last_error());
    else {
        $ls_velos = "";
        while ($l = pg_fetch_row($r))
        $ls_velos .= "<option value=\"" . $l[0] . "\"" . ($edit_id && $edit_values["id_velo"] === $l[0] ? " selected" : "") . ">" . $l[1] . "</option>";
        pg_free_result($r);
    }

    // recherche adherents
    $r = pg_query($conn, "SELECT id_adherent, nom_adherent, prenom_adherent FROM adherents;");
    if (!$r) $error_popup = generate_message(pg_last_error());
    else {
        $ls_adherents = "";
        while ($l = pg_fetch_row($r))
            $ls_adherents .= "<option value=\"" . $l[0] . "\"" . ($edit_id && $edit_values["id_adherent"] === $l[0] ? " selected" : "") . ">" . $l[1] . " " . $l[2] . "</option>";
        pg_free_result($r);
    }

    // recherche stations
    $r = pg_query($conn, "SELECT id_station, adresse_station, nom_ville FROM stations NATURAL JOIN communes;");
    if (!$r) $error_popup = generate_message(pg_last_error());
    else {
        $ls_station_departs = "";
        $ls_station_arrivees = "";
        while ($l = pg_fetch_row($r)) {
            $ls_station_departs .= "<option value=\"" . $l[0] . "\"" . ($edit_id && $edit_values["id_station_depart"] === $l[0] ? " selected" : "") . ">" . $l[1] . " (" . $l[2] . ")</option>";
            $ls_station_arrivees .= "<option value=\"" . $l[0] . "\"" . ($edit_id && $edit_values["id_station_arrivee"] === $l[0] ? " selected" : "") . ">" . $l[1] . " (" . $l[2] . ")</option>";
        }
        $ls_station_arrivees .= "<option value=\"null\"" . ($edit_id && $edit_values["id_station_arrivee"] === NULL ? " selected" : "") . "> - Non disponible - </option>";
        pg_free_result($r);
    }
} else { // sinon si c'est la vue normale, afficher les colonnes/lignes
    $result = pg_query($conn, "SELECT COUNT(*) FROM historique_utilisations");
    if (!$result) $error_popup = generate_message(pg_last_error());
    else {
        if (!($r = pg_fetch_row($result))) $error_popup = generate_message(pg_last_error());
        $nb_history = $r[0];

        pg_free_result($result);

        $query = "SELECT id_utilisation, date_debut_utilisation, date_fin_utilisation, reference,
                station_depart.adresse_station AS station_depart,
                COALESCE(station_arrivee.adresse_station, '- Non disponible') AS station_arrivee, nom_adherent, prenom_adherent,
                commune_depart.nom_ville AS commune_station_depart,
                commune_arrivee.nom_ville AS commune_station_arrivee
                FROM historique_utilisations
                NATURAL JOIN velos
                NATURAL JOIN adherents
                INNER JOIN stations AS station_depart
                ON (historique_utilisations.id_station_depart=station_depart.id_station)
                LEFT JOIN stations AS station_arrivee
                ON (historique_utilisations.id_station_arrivee=station_arrivee.id_station)
                INNER JOIN communes AS commune_depart
                ON (station_depart.id_commune=commune_depart.id_commune)
                LEFT JOIN communes AS commune_arrivee
                ON (station_arrivee.id_commune=commune_arrivee.id_commune)
                ORDER BY id_utilisation;";

        $result = pg_query($conn, $query);
        if (!$result) $error_popup = generate_message(pg_last_error());
        else {
            $tbody = "";

            while ($r = pg_fetch_assoc($result)) {
                $tbody .= "<tr>";
                $tbody .= "<td>" . $r["id_utilisation"] . "</td>";
                $date = explode(" ", $r["date_debut_utilisation"])[0];
                $tbody .= "<td>" . $date . "</td>";
                $start_time = explode(" ", $r["date_debut_utilisation"])[1];
                $start_time = explode(":", $start_time)[0] . ":" . explode(":", $start_time)[1];
                $tbody .= "<td>" . $start_time . "</td>";
                if (!empty($r["date_fin_utilisation"])) {
                    $end_time = explode(" ", $r["date_fin_utilisation"])[1];
                    $end_time = explode(":", $end_time)[0] . ":" . explode(":", $end_time)[1];
                    $tbody .= "<td>" . $end_time . "</td>";
                }
                else {
                    $tbody .= "<td>" . "N/A" . "</td>";
                }
                $tbody .= "<td>" . $r["reference"] . "</td>";
                $tbody .= "<td>" . $r["station_depart"] . " - " . $r["commune_station_depart"] .  "</td>";
                $tbody .= "<td>" . $r["station_arrivee"] . " - " . $r["commune_station_arrivee"] . "</td>";
                $tbody .= "<td>" . $r["nom_adherent"] . "</td>";
                $tbody .= "<td>" . $r["prenom_adherent"] . "</td>";
                $tbody .= "<td>" . createEditButton($r["id_utilisation"]) . "</td>";
                $tbody .= "<td>" . createDeleteButton($r["id_utilisation"]) . "</td>";
                $tbody .= "</tr>\n";
            }

            pg_free_result($result);
        }
    }
}

pg_close($conn);
?>
<div class="container">
    <h1>Historiques</h1>
    <!-- messages -->
    <?php echo empty($error_popup) ? $insertion_success : $error_popup; ?>
    <!-- ajouter le formulaire d'ajout/de modification si on le demande -->
    <?php if ($is_add_view || $edit_id ) { ?>
    <h2><?php echo $is_add_view ? "Ajouter un historique dans la base de données." : "Éditer un historique."; ?></h2>
    <form action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="POST">
        <?php if ($edit_id) { ?>
        <input type="hidden" name="edit_id" value="<?php echo $edit_id; ?>">
        <?php } ?>
        <input type="hidden" name="formname" value="<?php echo ($edit_id ? "edit" : "add"); ?>">
        <!-- date debut utilisation -->
        <label for="date_debut_utilisation" class="form-label">Date de début d'utilisation</label>
        <?php
        $format_datetime_start = "";
        if ($edit_id) {
            // formattage du temps pour la datetime-local
            $time = explode(" ", $edit_values["date_debut_utilisation"])[1];
            $format_datetime_start = explode(" ", $edit_values["date_debut_utilisation"])[0] . "T";
            $format_datetime_start .= explode(":", $time)[0] . ":" . explode(":", $time)[1];
        }
        ?>
        <input type="datetime-local" name="date_debut_utilisation" class="form-control mb-3" required value="<?php echo $format_datetime_start; ?>">
        <!-- date fin utilisation -->
        <label for="date_fin_utilisation" class="form-label">Date de fin d'utilisation</label>
        <?php
        $format_datetime_end = "";
        if ($edit_id && $edit_values["date_fin_utilisation"] !== NULL) {
            // formattage du temps pour la datetime-local
            $time = explode(" ", $edit_values["date_fin_utilisation"])[1];
            $format_datetime_end = explode(" ", $edit_values["date_fin_utilisation"])[0] . "T";
            $format_datetime_end .= explode(":", $time)[0] . ":" . explode(":", $time)[1];
        }
        ?>
        <input type="datetime-local" name="date_fin_utilisation" class="form-control mb-3" value="<?php echo $format_datetime_end; ?>">
        <!-- reference velo -->
        <label for="velo" class="form-label">Référence du vélo</label>
        <select name="velo" class="form-control mb-3" required value="<?php echo ($edit_id ? $edit_values["id_velo"] : ""); ?>"><?php echo $ls_velos; ?></select>
        <!-- adherent -->
        <label for="adherent" class="form-label">Adhérent</label>
        <select name="adherent" class="form-control mb-3" required value="<?php echo ($edit_id ? $edit_values["id_adherent"] : ""); ?>"><?php echo $ls_adherents; ?></select>
        <!-- station depart -->
        <label for="station_depart" class="form-label">Station de départ</label>
        <select name="station_depart" class="form-control mb-3" required value="<?php echo ($edit_id ? $edit_values["id_station_depart"] : ""); ?>"><<?php echo $ls_station_departs; ?></select>
        <!-- station arrivee -->
        <label for="station_arrivee" class="form-label">Station d'arrivée</label>
        <select name="station_arrivee" class="form-control mb-3" required><?php echo $ls_station_arrivees; ?></select>
        <button type="submit" class="btn btn-<?php echo ($edit_id ? "warning" : "primary"); ?> mb-3"><?php echo ($edit_id ? "Éditer" : "Ajouter"); ?></button>
    </form>
    <?php } else { ?>
    <!-- sinon, les colonnes/lignes -->
    <p>Il y a actuellement <b><?php echo $nb_history; ?></b> <?php echo $nb_history === "1" ? "historique" : "historiques"; ?> dans la base de données.</p>
    <?php echo createAddButton(); ?>
    <h2>Les historiques dans la base de données sont :</h2>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>ID</th>
                <th>Date d'utilisation</th>
                <th>Heure de début</th>
                <th>Heure de fin</th>
                <th>Référence du vélo</th>
                <th>Station de départ</th>
                <th>Station d'arrivée</th>
                <th>Nom de l'adhérent</th>
                <th>Prénom de l'adhérent</th>
                <th>Éditer</th>
                <th>Supprimer</th>
            </tr>
        </thead>
        <tbody>
            <?php echo $tbody; ?>
        </tbody>
    </table>
    <?php } ?>
</div>
