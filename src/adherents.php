<?php
$title = "Adhérents";
require_once './assets/php/header.php';
require_once './assets/php/connection.php';
require_once './assets/php/utils.php';

// variables
$is_add_view = false;
$edit_id = false;
$edit_values = [];
$insertion_success = "";
$error_popup = "";
$ls_communes = "";
$chercher_par_date = [];
$date_donnee_tmp = "";
$nb_member = 0;

// differentes operations
if ($_SERVER["REQUEST_METHOD"] === "POST") {
    switch ($_POST["formname"]) {
    case "delete":
        $r = pg_query_params($conn, 'DELETE FROM adherents WHERE id_adherent=$1;', array($_POST["id"]));
        if (!$r) $error_popup = generate_message(pg_last_error());
        else pg_free_result($r);
        break;
    case "pre_add":
        $is_add_view = true;
        break;
    case "add":
        $r = pg_query_params($conn, "
            INSERT INTO adherents(nom_adherent, prenom_adherent, adresse_adherent, date_adhesion, id_commune)
            VALUES ($1, $2, $3, $4, $5) RETURNING CURRVAL('adherents_id_adherent_seq');",
            array($_POST["nom_adherent"], $_POST["prenom_adherent"],
                    $_POST["adresse_adherent"], $_POST["date_adhesion"],
                    $_POST["commune"])
        );
        if (!$r) $error_popup = generate_message(pg_last_error());
        else {
            $insertion_success = generate_message("L'adhérent a bien été ajouté avec succès, avec l'identifiant " . pg_fetch_row($r)[0], "Succès !", "success");
            pg_free_result($r);
        }
        break;
    case "pre_edit":
        $edit_id = $_POST["id"];
        $r = pg_query_params($conn, "SELECT * FROM adherents WHERE id_adherent = $1;", array($edit_id));
        if (!$r) $error_popup = generate_message(pg_last_error());
        else {
            $edit_values = pg_fetch_assoc($r);
            if (!$edit_values) $error_popup = generate_message(pg_last_error());
        }
        pg_free_result($r);
        break;
    case "edit":
        $r = pg_query_params($conn, "
            UPDATE adherents SET nom_adherent = $1, prenom_adherent = $2, adresse_adherent = $3,
            date_adhesion = $4, id_commune = $5 WHERE id_adherent = $6;",
            array($_POST["nom_adherent"], $_POST["prenom_adherent"],
                    $_POST["adresse_adherent"], $_POST["date_adhesion"],
                    $_POST["commune"], $_POST["edit_id"])
        );
        if (!$r) $error_popup = generate_message(pg_last_error());
        else {
            $insertion_success = generate_message("La mise à jour s'est bien déroulée pour l'adhérent dont l'identifiant est " . $_POST["edit_id"], "Succès !", "success");
            pg_free_result($r);
        }
        break;
    case "chercher":
        $chercher_par_date = pg_query_params($conn, "SELECT ADHERENTS.*, COMMUNES.nom_ville
                                                    FROM ADHERENTS
                                                    NATURAL JOIN (SELECT DISTINCT date_debut_utilisation, id_velo, id_adherent FROM HISTORIQUE_UTILISATIONS) AS HISTORIQUE
                                                    NATURAL JOIN COMMUNES
                                                    WHERE HISTORIQUE.date_debut_utilisation::date = $1
                                                    GROUP BY ADHERENTS.id_adherent, COMMUNES.nom_ville
                                                    HAVING COUNT(HISTORIQUE.id_velo) >= 2;", array($_POST["date_donnee"]));
        if (!$chercher_par_date) $error_popup = generate_message(pg_last_error());
        $date_donnee_tmp = array($_POST["date_donnee"]);
        break;
    default:
        break;
    }
}

// si on ajoute ou modifie l'entite et qu'il y a des cles etrangeres, recuperer les valeurs
if ($is_add_view || $edit_id) {
    // recherche communes
    $r = pg_query($conn, "SELECT id_commune, nom_ville FROM communes;");
    if (!$r) $error_popup = generate_message(pg_last_error());
    else {
        $ls_communes = "";
        while ($l = pg_fetch_row($r))
            $ls_communes .= "<option value=\"" . $l[0] . "\"" . ($edit_id && $edit_values["id_commune"] === $l[0] ? "selected" : "") . ">" . $l[1] . "</option>";
        pg_free_result($r);
    }
} else { // sinon si c'est la vue normale, afficher les colonnes/lignes
    $result = pg_query($conn, "SELECT COUNT(*) FROM adherents");
    if (!$result) $error_popup = generate_message(pg_last_error());
    else {
        if (!($r = pg_fetch_row($result))) $error_popup = generate_message(pg_last_error());
        $nb_member = $r[0];

        pg_free_result($result);
        
        if ($chercher_par_date) $result = $chercher_par_date;
        else $result = pg_query($conn, "SELECT * FROM adherents NATURAL JOIN communes ORDER BY id_adherent");

        if (!$result) $error_popup = generate_message(pg_last_error());
        else {
            $tbody = "";
            while ($r = pg_fetch_assoc($result)) {
                $tbody .= "<tr>";
                $tbody .= "<td>" . $r["id_adherent"] . "</td>";
                $tbody .= "<td>" . $r["nom_adherent"] . "</td>";
                $tbody .= "<td>" . $r["prenom_adherent"] . "</td>";
                $tbody .= "<td>" . $r["adresse_adherent"] . "</td>";
                $tbody .= "<td>" . $r["date_adhesion"] . " </td>";
                $tbody .= "<td>" . $r["nom_ville"] . " </td>";
                if (!$chercher_par_date) {
                    $tbody .= "<td>" . createEditButton($r["id_adherent"]) . "</td>";
                    $tbody .= "<td>" . createDeleteButton($r["id_adherent"]) . "</td>";
                }
                $tbody .= "</tr>\n";
            }
            pg_free_result($result);
        }
    }
}
pg_close($conn);
?>

<div class="container">
    <h1>Adhérents</h1>
    <!-- messages -->
    <?php echo empty($error_popup) ? $insertion_success : $error_popup; ?>
    <!-- ajouter le formulaire d'ajout/de modification si on le demande -->
    <?php if ($is_add_view || $edit_id ) { ?>
    <h2><?php echo $is_add_view ? "Ajouter un adhérent dans la base de données." : "Éditer un adhérent."; ?></h2>
    <form action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="POST">
        <?php if ($edit_id) { ?>
        <input type="hidden" name="edit_id" value="<?php echo $edit_id; ?>">
        <?php } ?>
        <input type="hidden" name="formname" value="<?php echo ($edit_id ? "edit" : "add"); ?>">
        <!-- nom adherent -->
        <label for="nom_adherent" class="form-label">Nom</label>
        <input type="text" name="nom_adherent" placeholder="Nom" class="form-control mb-3" required value="<?php echo ($edit_id ? $edit_values["nom_adherent"] : ""); ?>">
        <!-- prenom adherent -->
        <label for="prenom_adherent" class="form-label">Prénom</label>
        <input type="text" name="prenom_adherent" placeholder="Prénom" class="form-control mb-3" required value="<?php echo ($edit_id ? $edit_values["prenom_adherent"] : ""); ?>">
        <!-- adresse adherent -->
        <label for="adresse_adherent" class="form-label">Adresse</label>
        <input type="text" class="form-control mb-3" name="adresse_adherent" placeholder="Adresse" required value="<?php echo ($edit_id ? $edit_values["adresse_adherent"] : ""); ?>">
        <!-- date adhesion -->
        <label for="date_adhesion" class="form-label">Date d'adhésion</label>
        <input type="date" min="0" max="100" class="form-control mb-3" name="date_adhesion" required value="<?php echo ($edit_id ? $edit_values["date_adhesion"] : ""); ?>">
        <!-- commune -->
        <label for="id_commune" class="form-label">Commune</label>
        <select name="commune" class="form-control mb-3" required value="<?php echo ($edit_id ? $edit_values["id_commune"] : ""); ?>"><?php echo $ls_communes;?></select>
        <button type="submit" class="btn btn-<?php echo ($edit_id ? "warning" : "primary"); ?> mb-3"><?php echo ($edit_id ? "Éditer" : "Ajouter"); ?></button>
    </form>
    <?php } else { ?>
    <!-- sinon, les colonnes/lignes -->
    <p>Il y a actuellement <b><?php echo $nb_member; ?></b> <?php echo $nb_member === "1" ? "adhérent" : "adhérents"; ?> dans la base de données.</p>
    <div class="row mb-3 row-cols-auto">
        <div><?php echo createAddButton(); ?></div>
        <form action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="POST">
            <input type="date" name="date_donnee" class="form-control mb-1" required value="<?php if (isset($date_donnee_tmp[0])) echo $date_donnee_tmp[0] ?>">
            <input type="hidden" name="formname" value="chercher"/>
            <button type="submit" class="btn btn-info">Chercher les adhérents ayant pris au moins deux vélos distinct pour un jour donné</button>
        </form>
    </div>
    <?php
    if ($chercher_par_date)
        echo "<h2>Voici les adhérents ayant emprunté au moins deux vélos distinct le " . $date_donnee_tmp[0] . " :</h2>";
    else
        echo "<h2>Les adhérents dans la base de données sont :</h2>";
    ?>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>ID</th>
                <th>Nom</th>
                <th>Prénom</th>
                <th>Adresse</th>
                <th>Date d'adhésion</th>
                <th>Habite à</th>
                <?php 
                if (!$chercher_par_date)
                    echo "<th>Éditer</th><th>Supprimer</th>";
                ?>
            </tr>
        </thead>
        <tbody>
            <?php echo $tbody; ?>
        </tbody>
    </table>
    <?php } ?>
</div>
