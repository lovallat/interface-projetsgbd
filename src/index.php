<?php
$title = "Accueil";
require_once './assets/php/header.php';
require_once './assets/php/connection.php';
require_once './assets/php/utils.php';

$info = "";

if ($_SERVER["REQUEST_METHOD"] === "POST") {
    $base_sql = file_get_contents( '../sql/base.sql');
    $data_sql = file_get_contents('../sql/data.sql');
    if (!$base_sql || !$data_sql) $error_popup = generate_message("Un des deux fichiers base.sql ou data.sql est manquant dans le dossier ../sql/");
    else {
        if (isset($_POST["flush"])) {
            $r = pg_query($conn, $base_sql);
            if (!$r) $error_popup = generate_message(pg_last_error());
            else {
                $info = generate_message("La remise à zéro s'est bien passée", "Succès !", "success");
                pg_free_result($r);
            }
        } else if (isset($_POST["fill"])) {
            $r = pg_query($conn, $base_sql);
            if (!$r) $error_popup = generate_message(pg_last_error());
            else {
            pg_free_result($r);
            $r = pg_query($conn, $data_sql);
            if (!$r) $error_popup = generate_message(pg_last_error());
            else {
                $info = generate_message("La remise à zéro de la base avec les valeurs par défaut s'est bien passé", "Succès !", "success");
                pg_free_result($r);
            }
            }
        }
    }
}

pg_close($conn);
?>
    <div class="container">
        <h1>Bienvenue</h1>
        <!-- messages -->
        <?php echo empty($error_popup) ? $info : $error_popup; ?>
        <h2>Vider ou remplir la base de données.</h2>
        <form action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="post">
            <button type="submit" class="btn btn-danger" name="flush">Remettre à zéro</button>
            <button type="submit" class="btn btn-success" name="fill">Remettre à zéro et charger le jeu de tests</button>
        </form>
    </div>
</body>
</html>
