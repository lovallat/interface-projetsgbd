<?php
$title = "Stations";
require_once './assets/php/header.php';
require_once './assets/php/connection.php';
require_once './assets/php/utils.php';

// variables
$is_add_view = false;
$edit_id = false;
$edit_values = [];
$insertion_success = "";
$error_popup = "";
$ls_communes = "";
$chercher_par_commune = [];
$commune_donnee = "";
$nb_station = 0;


// differentes operations
if ($_SERVER["REQUEST_METHOD"] === "POST") {
    switch ($_POST["formname"]) {
    case "delete":
        $r = pg_query_params($conn, 'DELETE FROM stations WHERE id_station=$1;', array($_POST["id"]));
        if (!$r) $error_popup = generate_message(pg_last_error());
        else pg_free_result($r);
        break;
    case "pre_add":
        $is_add_view = true;
        break;
    case "add":
        $r = pg_query_params($conn, "
            INSERT INTO stations(adresse_station, nombre_bornes, id_commune)
            VALUES ($1, $2, $3) RETURNING CURRVAL('stations_id_station_seq');",
            array($_POST["adresse_station"], $_POST["nombre_bornes"],
                    $_POST["commune"])
        );
        if (!$r) $error_popup = generate_message(pg_last_error());
        else {
            $insertion_success = generate_message("La station a bien été ajouté avec succès, avec l'identifiant " . pg_fetch_row($r)[0], "Succès !", "success");
            pg_free_result($r);
        }
        break;
    case "pre_edit":
        $edit_id = $_POST["id"];
        $r = pg_query_params($conn, "SELECT * FROM stations WHERE id_station = $1;", array($edit_id));
        if (!$r) $error_popup = generate_message(pg_last_error());
        else {
            $edit_values = pg_fetch_assoc($r);
            if (!$edit_values) $error_popup = generate_message(pg_last_error());
            pg_free_result($r);
        }
        break;
    case "edit":
        $r = pg_query_params($conn, "
            UPDATE stations SET adresse_station = $1, nombre_bornes = $2, id_commune = $3 
            WHERE id_station = $4;",
            array($_POST["adresse_station"], $_POST["nombre_bornes"],
                    $_POST["commune"], $_POST["edit_id"])
        );
        if (!$r) $error_popup = generate_message(pg_last_error());
        else {
            $insertion_success = generate_message("La mise à jour s'est bien déroulée pour la station dont l'identifiant est " . $_POST["edit_id"], "Succès !", "success");
            pg_free_result($r);
        }
        break;
    case "chercher":
        $chercher_par_commune = pg_query_params($conn, "SELECT STATIONS.id_station, STATIONS.adresse_station, STATIONS.nombre_bornes, COMMUNES.nom_ville
                                FROM STATIONS
                                NATURAL JOIN COMMUNES
                                WHERE id_commune = $1;", array($_POST["commune"]));
        if (!$chercher_par_commune) $error_popup = generate_message(pg_last_error());
        break;
    default:
        break;
    }
}

// recuperation des communes
$r = pg_query($conn, "SELECT id_commune, nom_ville FROM communes;");
if (!$r) $error_popup = generate_message(pg_last_error());
else {
    $ls_communes = "";
    while ($l = pg_fetch_row($r)) {
        if (isset($_POST["commune"]) && $_POST["commune"] === $l[0]) $commune_donnee = $l[1];
        $ls_communes .= "<option value=\"" . $l[0] . "\"" . ($edit_id && $edit_values["id_commune"] === $l[0] ? " selected" : (isset($_POST["commune"]) && $_POST["commune"] === $l[0] ? " selected" : "")) . ">" . $l[1] . "</option>";
    }
    pg_free_result($r);
}

if (!$edit_id && !$is_add_view) { // si c'est la vue normale, afficher les colonnes/lignes
    $result = pg_query($conn, "SELECT COUNT(*) FROM stations");
    if (!$result) $error_popup = generate_message(pg_last_error());
    else {
        if (!($r = pg_fetch_row($result))) $error_popup = generate_message(pg_last_error());
        $nb_station = $r[0];

        pg_free_result($result);

        if ($chercher_par_commune) $result = $chercher_par_commune;
        else $result = pg_query($conn, "SELECT * FROM stations NATURAL JOIN communes ORDER BY id_station");

        if (!$result) $error_popup = generate_message(pg_last_error());
        else {
            $tbody = "";

            while ($r = pg_fetch_assoc($result)) {
                $tbody .= "<tr>";
                $tbody .= "<td>" . $r["id_station"] . "</td>";
                $tbody .= "<td>" . $r["adresse_station"] . "</td>";
                $tbody .= "<td>" . $r["nombre_bornes"] . "</td>";
                $tbody .= "<td>" . $r["nom_ville"] . "</td>";
                if (!$chercher_par_commune) {
                    $tbody .= "<td>" . createEditButton($r["id_station"]) . "</td>";
                    $tbody .= "<td>" . createDeleteButton($r["id_station"]) . "</td>";
                }
                $tbody .= "</tr>\n";
            }

            pg_free_result($result);
        }
    }
}

pg_close($conn);
?>
<div class="container">
    <h1>Stations</h1>
    <!-- messages -->
    <?php echo empty($error_popup) ? $insertion_success : $error_popup; ?>
    <!-- ajouter le formulaire d'ajout/de modification si on le demande -->
    <?php if ($is_add_view || $edit_id ) { ?>
    <h2><?php echo $is_add_view ? "Ajouter une station dans la base de données." : "Éditer une station."; ?></h2>
    <form action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="POST">
        <?php if ($edit_id) { ?>
        <input type="hidden" name="edit_id" value="<?php echo $edit_id; ?>">
        <?php } ?>
        <input type="hidden" name="formname" value="<?php echo ($edit_id ? "edit" : "add"); ?>">
        <!-- adresse station -->
        <label for="adresse_station" class="form-label">Adresse</label>
        <input type="text" name="adresse_station" placeholder="Adresse" class="form-control mb-3" required value="<?php echo ($edit_id ? $edit_values["adresse_station"] : ""); ?>">
        <!-- nombre de bornes -->
        <label for="nombre_bornes" class="form-label">Nombre de bornes</label>
        <input type="number" name="nombre_bornes" placeholder="50" class="form-control mb-3" required value="<?php echo ($edit_id ? $edit_values["nombre_bornes"] : ""); ?>">
        <!-- commune -->
        <label for="commune" class="form-label">Commune</label>
        <select name="commune" class="form-control mb-3" required value="<?php echo ($edit_id ? $edit_values["id_commune"] : ""); ?>"><?php echo $ls_communes;?></select>
        <button type="submit" class="btn btn-<?php echo ($edit_id ? "warning" : "primary"); ?> mb-3"><?php echo ($edit_id ? "Éditer" : "Ajouter"); ?></button>
    </form>
    <?php } else { ?>
    <!-- sinon, les colonnes/lignes -->
    <p>Il y a actuellement <b><?php echo $nb_station; ?></b> <?php echo $nb_station === "1" ? "station" : "stations"; ?> dans la base de données.</p>
    <div class="row mb-3 row-cols-auto">
        <div><?php echo createAddButton(); ?></div>
        <form action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="POST">
            <select name="commune" class="form-control mb-1" required><?php echo $ls_communes ?></select>
            <input type="hidden" name="formname" value="chercher"/>
            <button type="submit" class="btn btn-info">Chercher par commune</button>
        </form>
    </div>
    <?php
    if ($chercher_par_commune)
        echo "<h2>Voici les stations se situant dans la commune " . $commune_donnee . " :</h2>";
    else
        echo "<h2>Les stations dans la base de données sont :</h2>";
    ?>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>ID</th>
                <th>Adresse</th>
                <th>Nombre de bornes</th>
                <th>Commune</th>
                <?php
                if (!$chercher_par_commune)
                    echo "<th>Éditer</th><th>Supprimer</th>";
                ?>
            </tr>
        </thead>
        <tbody>
            <?php echo $tbody; ?>
        </tbody>
    </table>
    <?php } ?>
</div>
