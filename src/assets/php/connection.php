<?php
// CONNEXION A LA BASE DE DONNEES
$username = getenv("DB_USERNAME");
$password = getenv("DB_PASSWORD");
$port = getenv("DB_PORT");
$host = getenv("DB_HOST");
$dbname = getenv("DB_NAME");
$conn_string = "host=$host dbname=$dbname port=$port user=$username password=$password";
$conn = pg_connect($conn_string);
if (!$conn) {
    echo "<div class=\"alert alert-danger\" role=\"alert\">
            <h4 class=\"alert-heading\">Attention !</h4>
            <p>Il y a eu une erreur de connexion à la base de données.
            <br>Veuillez vérifier votre identifiant, mot de passe ainsi que le nom de la base de données</p>
            <p>Si le problème persiste, veuillez nous contacter</p>
            <p>Indice pour le lancement du serveur php : DB_USERNAME=identifiant DB_PASSWORD=motDePasse DB_PORT=33548 DB_HOST=louis-vallat.xyz DB_NAME=nomBase php -S localhost:8000</p>
            </div>";
}
pg_set_error_verbosity($conn, PGSQL_ERRORS_TERSE);
?>
