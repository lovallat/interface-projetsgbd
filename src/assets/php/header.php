<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title><?php echo $title; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>
<body>
    <header class="d-flex justify-content-center py-3 border-bottom container">
        <ul class="nav nav-pills">
            <li class="nav-item"><a class="nav-link <?php echo ($_SERVER['PHP_SELF'] === "/index.php" ? "active" : ""); ?>" href="/index.php">Accueil</a></li>
            <li class="nav-item"><a class="nav-link <?php echo ($_SERVER['PHP_SELF'] === "/velos.php" ? "active" : ""); ?>" href="/velos.php">Vélos</a></li>
            <li class="nav-item"><a class="nav-link <?php echo ($_SERVER['PHP_SELF'] === "/stations.php" ? "active" : ""); ?>" href="/stations.php">Stations</a></li>
            <li class="nav-item"><a class="nav-link <?php echo ($_SERVER['PHP_SELF'] === "/adherents.php" ? "active" : ""); ?>" href="/adherents.php">Adhérents</a></li>
            <li class="nav-item"><a class="nav-link <?php echo ($_SERVER['PHP_SELF'] === "/communes.php" ? "active" : ""); ?>" href="/communes.php">Communes</a></li>
            <li class="nav-item"><a class="nav-link <?php echo ($_SERVER['PHP_SELF'] === "/marques.php" ? "active" : ""); ?>" href="/marques.php">Marques</a></li>
            <li class="nav-item"><a class="nav-link <?php echo ($_SERVER['PHP_SELF'] === "/historiques.php" ? "active" : ""); ?>" href="/historiques.php">Historiques</a></li>
            <li class="nav-item"><a class="nav-link <?php echo ($_SERVER['PHP_SELF'] === "/distance.php" ? "active" : ""); ?>" href="/distance.php">Distances</a></li>
            <li class="nav-item"><a class="nav-link <?php echo ($_SERVER['PHP_SELF'] === "/stats.php" ? "active" : ""); ?>" href="/stats.php">Statistiques</a></li>
        </ul>
    </header>
