<?php

/**
 * Creates a delete button, the id corresponding to the item to delete from the database
 */
function createDeleteButton($id) {
    $f = "<form action=\"" . $_SERVER["PHP_SELF"] . "\" method=\"post\">";
    $f .= "<button type=\"submit\" class=\"btn btn-danger btn-sm\" name=\"id\" value=\"" . $id ."\">Supprimer</button>";
    $f .= "<input type=\"hidden\" name=\"formname\" value=\"delete\"/>";
    $f .= "</form>";
    return $f;
}

/**
 * Creates an add button
 */
function createAddButton() {
    $f = "<form action=\"" . $_SERVER["PHP_SELF"] . "\" method=\"post\" class=\"mb-3\">";
    $f .= "<button type=\"submit\" class=\"btn btn-success\">Ajouter</button>";
    $f .= "<input type=\"hidden\" name=\"formname\" value=\"pre_add\"/>";
    $f .= "</form>";
    return $f;
}

/**
 * Creates an edit button, the id corresponding to the item to edit in the database
 */
function createEditButton($id) {
    $f = "<form action=\"" . $_SERVER["PHP_SELF"] . "\" method=\"post\">";
    $f .= "<button type=\"submit\" class=\"btn btn-warning btn-sm\" name=\"id\" value=\"" . $id ."\">Modifier</button>";
    $f .= "<input type=\"hidden\" name=\"formname\" value=\"pre_edit\"/>";
    $f .= "</form>";
    return $f;
}

/**
 * Creates a consultation button
 */
function createConsultationButton($button_name) {
    $f = "<form action=\"" . $_SERVER["PHP_SELF"] . "\" method=\"post\">";
    $f .= "<button type=\"submit\" class=\"btn btn-info\">" . $button_name . "</button>";
    $f .= "<input type=\"hidden\" name=\"formname\" value=\"$button_name\"/>";
    $f .= "</form>";
    return $f;
}

/**
 * Checks if a bike is in use if someone wants to use it
 */
function checkIfBikeIsInUse($conn, $bikeID, $arrival_station) {
    $check_bike_isnt_in_use = pg_query_params($conn, "SELECT count(*) FROM historique_utilisations WHERE id_velo = $1 AND id_station_arrivee IS NULL", array($bikeID));
    if (!$check_bike_isnt_in_use) $error_popup = generate_message(pg_last_error());
    $result = pg_fetch_assoc($check_bike_isnt_in_use)["count"] > 0 && $arrival_station === "null";
    pg_free_result($check_bike_isnt_in_use);
    return $result;
}

/**
 * Checks if the end date and arrival station are either both null or both non null
 */
function checkIfBothEndDateAndStationNull($end_date, $arrival_station) {
    return (!empty($end_date) && $arrival_station === "null") || (empty($end_date) && $arrival_station !== "null");
}

/**
 * Checks if a given start date is superior to a given end date
 */
function checkStartSuperiorEnd($start_date, $end_date) {
    return $start_date > $end_date && !empty($end_date);
}

/**
 * Checks if a start day is equal to an end day
 */
function checkStartSameDayEnd($start_date, $end_date) {
    $start = explode("T", $start_date)[0];
    $end = explode("T", $end_date)[0];
    return $start != $end && !empty($end_date);
}

/**
 * Checks if a bike usage doesn't overlap with another current usage
 */
function checkDateForUsage($conn, $bikeID, $start_date, $end_date, $arrival_station, $historyID) {
    $r = pg_query_params($conn, "SELECT date_debut_utilisation, id_utilisation FROM historique_utilisations WHERE id_velo = $1 AND id_station_arrivee IS NULL ORDER BY date_debut_utilisation", array($bikeID));
    if (!$r) $error_popup = generate_message(pg_last_error());
    $get_most_recent_usage = pg_fetch_row($r);
    pg_free_result($r);
    return !empty($get_most_recent_usage[1]) && (($historyID === NULL || $historyID !== $get_most_recent_usage[1]) && !empty($get_most_recent_usage[0]) && ($start_date >= $get_most_recent_usage[0] || ($arrival_station === "null" || $end_date >= $get_most_recent_usage[0])));
}

/**
 * Checks if the arrival station isn't already full
 */
function checkIfPlacesLeft($conn, $arrival_station, $start_date) {
    if (explode("T", $start_date)[0] === date("Y-m-d")) {
        if ($arrival_station !== "null") {
            $r = pg_query_params($conn, "SELECT nbr_places_disponibles
                                        FROM (SELECT STATIONS.id_station, STATIONS.nombre_bornes - COUNT(VELOS.id_velo) AS nbr_places_disponibles 
                                                FROM STATIONS 
                                                NATURAL LEFT JOIN VELOS
                                                GROUP BY STATIONS.id_station, STATIONS.id_commune) AS STATIONS
                                        WHERE id_station = $1", array($arrival_station));
            if (!$r) $error_popup = generate_message(pg_last_error());
            $places_left = pg_fetch_row($r)[0];
            pg_free_result($r);
            return $places_left === "0";
        }
    }
    return false;
}

/**
 * Checks that the starting station matches the bike's starting station
 * Only used for adding as when editing there's no way of knowing the original station (in the event that the station was changed manually)
 */
function checkStartingStations($conn, $bikeID, $starting_station, $date) {
    if (explode("T", $date)[0] === date("Y-m-d")) {
        $r = pg_query_params($conn, "SELECT id_station FROM velos WHERE id_velo = $1", array($bikeID));
        if (!$r) $error_popup = generate_message(pg_last_error());
        $current_station = pg_fetch_row($r);
        pg_free_result($r);
        return $current_station[0] !== $starting_station;
    }
    return false;
}

/**
 * Checks that the date_mise_en_service date in velos is inferior to usage date
 */
function checkPutIntoServiceDate($conn, $bikeID, $start_date) {
    $r = pg_query_params($conn, "SELECT date_mise_en_service FROM velos WHERE id_velo = $1", array($bikeID));
    if (!$r) $error_popup = generate_message(pg_last_error());
    $service_date = pg_fetch_row($r);
    pg_free_result($r);
    return $service_date[0] > $start_date;
}

/**
 * Checks that the admission date is inferior to usage date
 */
function checkAdmissionDate($conn, $memberID, $start_date) {
    $r = pg_query_params($conn, "SELECT date_adhesion FROM adherents WHERE id_adherent = $1", array($memberID));
    if (!$r) $error_popup = generate_message(pg_last_error());
    $admission_date = pg_fetch_row($r);
    pg_free_result($r);
    return $admission_date[0] > $start_date;
}

/**
 * Checks that you cant add usages in the future
 */
function checkNotInTheFuture($conn, $start_date) {
    return explode("T", $start_date)[0] > date("Y-m-d");
}

/**
 * Checks that if bike batterie is below 10, the bike cant be taken
 */
function checkBikeBattery($conn, $bikeID) {
    $r = pg_query_params($conn, "SELECT niveau_charge_batterie FROM velos WHERE id_velo = $1", array($bikeID));
    if (!$r) $error_popup = generate_message(pg_last_error());
    $niveau_charge = pg_fetch_row($r);
    pg_free_result($r);
    return $niveau_charge[0] <= 10;
}

function generate_message($msg, $title = "Attention !", $type = "danger") {
    $edited_msg = $type === "danger" ? explode("ERROR:", explode("DETAIL:", explode("CONTEXT:", $msg)[0])[0])[1] : $msg; 
    if (str_contains($edited_msg, "date_mise_en_service_pas_futur")) // date de mise en service dans le futur
        $edited_msg = "La date de mise en service d'un vélo ne peut pas être dans le futur";
    else if (str_contains($edited_msg, "date_debut_pas_futur")) // date de debut dans le futur
        $edited_msg = "La date de début d'une utilisation ne peut pas être dans le futur";
    else if (str_contains($edited_msg, "date_adhesion_pas_futur")) // date d'adhesion dans le futur
        $edited_msg = "La date d'adhésion d'un adhérent ne peut pas être dans le futur";
    else if (str_contains($edited_msg, "date_fin_sup_date_debut")) // date fin < date debut
        $edited_msg = "La date de début doit être inférieur à la date de fin";
    else if (str_contains($edited_msg, "jour_debut_eq_jour_fin")) // jour debut == jour fin
        $edited_msg = "Le jour de début doit être égal au jour de fin (l'heure peut être différente)";
    else if (str_contains($edited_msg, "date_fin_et_st_arr_null_ou_non_null")) // (fin == null && arrivee == null) || (fin != null && arrivee != null)
        $edited_msg = "Soit la station d'arrivée et la date de fin sont tous les deux nulls, soit ils sont tous les deux non null";
    return "
        <div class=\"alert alert-$type\" role=\"alert\">
            <h4 class=\"alert-heading\">$title</h4>
            <p>$edited_msg</p>
        </div>
        ";

}

?>
