<?php
$title = "Statistiques";
require_once './assets/php/header.php';
require_once './assets/php/connection.php';
require_once './assets/php/utils.php';

// variables
$error_popup = "";
$moyenne_velos_par_jour = 0;
$moyenne_velos_par_sur_semaine = 0;
$classement_charge_velos = "";
$classement_stations = "";

// moyennes du nombre d'usagers par velo par jour
$result = pg_query($conn, 'SELECT AVG(avg_jour)
                FROM (SELECT CAST(COUNT(*) AS FLOAT) / CAST((SELECT COUNT(*) FROM VELOS) AS FLOAT) as avg_jour
                FROM (SELECT DISTINCT HISTORIQUE_UTILISATIONS.date_debut_utilisation::date, HISTORIQUE_UTILISATIONS.id_adherent 
                FROM HISTORIQUE_UTILISATIONS) AS HISTORIQUE
                GROUP BY HISTORIQUE.date_debut_utilisation::date) AS moyenne_velos_par_jours;');
if (!$result) $error_popup = generate_message(pg_last_error());
else {
    if (!($moyenne_velos_par_jour = pg_fetch_row($result))) $error_popup = generate_message(pg_last_error());
    pg_free_result($result);
}

// classement des velos les plus charges par stations
$result = pg_query($conn, 'SELECT VELOS.id_velo, VELOS.reference, VELOS.date_mise_en_service, VELOS.etat, VELOS.niveau_charge_batterie, 
                MARQUES.nom_marque, STATIONS.adresse_station, COMMUNES.nom_ville
                FROM STATIONS 
                NATURAL JOIN VELOS
                NATURAL JOIN MARQUES
                NATURAL JOIN COMMUNES
                ORDER BY STATIONS.id_station ASC, VELOS.niveau_charge_batterie DESC;');
if (!$result) $error_popup = generate_message(pg_last_error());
else {
    $classement_charge_velos = "";
    while ($r = pg_fetch_assoc($result)) {
        $classement_charge_velos .= "<tr>";
        $classement_charge_velos .= "<td>" . $r["id_velo"] . "</td>";
        $classement_charge_velos .= "<td>" . $r["reference"] . "</td>";
        $classement_charge_velos .= "<td>" . $r["date_mise_en_service"] . "</td>";
        $classement_charge_velos .= "<td>" . $r["etat"] . "</td>";
        $classement_charge_velos .= "<td>" . $r["niveau_charge_batterie"] . "</td>";
        $classement_charge_velos .= "<td>" . $r["nom_marque"] . "</td>";
        $classement_charge_velos .= "<td>" . $r["adresse_station"] . "</td>";
        $classement_charge_velos .= "<td>" . $r["nom_ville"] . "</td>";
        $classement_charge_velos .= "</tr>\n";
    }
    pg_free_result($result);
}

// moyenne des distances parcourues par les vélos sur une semaine
$date = isset($_POST["date_donnee"]) ? $_POST["date_donnee"] : date("Y-m-d");
$result = pg_query_params($conn, "SELECT CAST(SUM(ETRE_DISTANTE_DE.distance) AS FLOAT) / 7
                FROM (SELECT * 
                FROM HISTORIQUE_UTILISATIONS 
                WHERE DATE_PART('week', HISTORIQUE_UTILISATIONS.date_debut_utilisation) = DATE_PART('week', date($1))) AS HISTORIQUE
                JOIN ETRE_DISTANTE_DE 
                ON (HISTORIQUE .id_station_depart = ETRE_DISTANTE_DE.id_station_1 AND HISTORIQUE .id_station_arrivee = ETRE_DISTANTE_DE.id_station_2);", array($date));
if (!$result) $error_popup = generate_message(pg_last_error());
else {
    if (!($moyenne_velos_par_sur_semaine = pg_fetch_row($result))) $error_popup = generate_message(pg_last_error());
    pg_free_result($result);
}

// classement des stations par nombre de places disponibles par commune
$result = pg_query($conn, "SELECT COMMUNES.nom_ville, STAT.adresse_station, STAT.nbr_places_disponibles
                FROM (SELECT * 
                    FROM (SELECT STATIONS.id_commune, STATIONS.adresse_station, STATIONS.nombre_bornes - COUNT(VELOS.id_velo) AS nbr_places_disponibles 
                    FROM STATIONS 
                    NATURAL LEFT JOIN VELOS 
                    GROUP BY STATIONS.id_station, STATIONS.id_commune, STATIONS.adresse_station) AS STATIONS
                    ORDER BY STATIONS.id_commune ASC, STATIONS.nbr_places_disponibles ASC) AS STAT
                    NATURAL JOIN COMMUNES;");
if (!$result) $error_popup = generate_message(pg_last_error());
else {
    $classement_stations = "";
    while ($r = pg_fetch_assoc($result)) {
        $classement_stations .= "<tr>";
        $classement_stations .= "<td>" . $r["nom_ville"] . "</td>";
        $classement_stations .= "<td>" . $r["adresse_station"] . "</td>";
        $classement_stations .= "<td>" . $r["nbr_places_disponibles"] . "</td>";
        $classement_stations .= "</tr>\n";
    }
    pg_free_result($result);
}
?>

<div class="container">
    <h1>Statistiques</h1>
    <!-- messages -->
    <?php echo $error_popup; ?>
    <h3>Moyenne du nombre d'usagers par vélo par jour</h3>
    <p>La moyenne est d'environ <strong><?php echo str_split($moyenne_velos_par_jour[0], 5)[0]; ?></strong> usagers par vélo par jour.</p>
    <h3>Moyenne des distances parcourues par les vélos sur une semaine</h3>
    <p>La moyenne est d'environ <strong><?php echo explode(".", $moyenne_velos_par_sur_semaine[0])[0]; ?>m</strong> de distance dans la semaine.</p>
    <div class="row row-cols-auto">
        <form action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="POST" class="col">
            <input type="date" name="date_donnee" class="form-control mb-1" required value="<?php echo $date; ?>">
            <input type="hidden" name="formname" value="chercher"/>
            <button type="submit" class="btn btn-info mb-3">Moyenne de vélo sur une semaine</button>
        </form>
    </div>

    <h3>Le classement des vélos par niveau de charge par station</h3>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>ID</th>
                <th>Référence</th>
                <th>Date de mise en service</th>
                <th>Etat</th>
                <th>Niveau de charge de la batterie</th>
                <th>Marque</th>
                <th>Adresse</th>
                <th>Commune</th>
            </tr>
        </thead>
        <tbody>
            <?php echo $classement_charge_velos; ?>
        </tbody>
    </table>
    <h3>Le classement des stations par places disponibles</h3>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Commune</th>
                <th>Adresse</th>
                <th>Places disponibles</th>
            </tr>
        </thead>
        <tbody>
            <?php echo $classement_stations; ?>
        </tbody>
    </table>
</div>