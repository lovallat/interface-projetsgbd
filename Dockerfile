FROM registry.gitlab.com/lovallat/php-apache-pagespeed:latest

RUN apt update \
    && apt install -y libpq-dev \
    && rm -rf /var/lib/apt/lists/* \
    && docker-php-ext-install -j$(nproc) pdo_pgsql pgsql

EXPOSE 80

COPY --chown=www-data:www-data ./src/ /var/www/html/
COPY --chown=www-data:www-data ./sql/ /var/www/sql/

