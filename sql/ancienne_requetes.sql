-- 4.2 TRIGGERS SUR LA SUPPRESION

-- suppresion commune
CREATE OR REPLACE FUNCTION communes_verif() RETURNS TRIGGER AS $trigger_suppr_communes_verif$
DECLARE
    st RECORD;
    ad RECORD;
BEGIN
    FOR st IN SELECT id_station
                FROM STATIONS
                WHERE STATIONS.id_commune = OLD.id_commune
    LOOP
		-- suppression historiques associes
		DELETE FROM HISTORIQUE_UTILISATIONS
		WHERE HISTORIQUE_UTILISATIONS.id_station_depart = st.id_station
		OR HISTORIQUE_UTILISATIONS.id_station_arrivee = st.id_station;
		-- suppression distances associes
		DELETE FROM ETRE_DISTANTE_DE
		WHERE ETRE_DISTANTE_DE.id_station_1 = st.id_station
		OR ETRE_DISTANTE_DE.id_station_2 = st.id_station;
		-- suppression velos associes
		DELETE FROM VELOS
		WHERE VELOS.id_station = st.id_station;
		-- suppression stations
        DELETE FROM STATIONS
        WHERE STATIONS.id_station = st.id_station;
    END LOOP;
    FOR ad IN SELECT id_adherent
                FROM ADHERENTS
                WHERE ADHERENTS.id_commune = OLD.id_commune
    LOOP    
        -- suppression historiques associes
        DELETE FROM HISTORIQUE_UTILISATIONS
        WHERE HISTORIQUE_UTILISATIONS.id_adherent = ad.id_adherent;
        -- suppression adherents
        DELETE FROM ADHERENTS
        WHERE ADHERENTS.id_adherent = ad.id_adherent;
    END LOOP;
	RETURN OLD;
END;    
$trigger_suppr_communes_verif$ LANGUAGE plpgsql; 

CREATE OR REPLACE TRIGGER trigger_suppr_communes_verif
BEFORE DELETE ON COMMUNES
FOR EACH ROW
EXECUTE FUNCTION communes_verif();

-- suppresion station
CREATE OR REPLACE FUNCTION stations_verif() RETURNS TRIGGER AS $trigger_suppr_stations_verif$
DECLARE
    hi RECORD;
    ve RECORD;
    di RECORD;
BEGIN
    FOR hi IN SELECT id_utilisation
                FROM HISTORIQUE_UTILISATIONS
                WHERE HISTORIQUE_UTILISATIONS.id_station_depart = OLD.id_station
                OR HISTORIQUE_UTILISATIONS.id_station_arrivee = OLD.id_station
    LOOP
        -- suppression historiques
        DELETE FROM HISTORIQUE_UTILISATIONS
        WHERE HISTORIQUE_UTILISATIONS.id_utilisation = hi.id_utilisation;
    END LOOP;
    FOR ve IN SELECT id_velo
                FROM VELOS
                WHERE VELOS.id_station = OLD.id_station
    LOOP
        -- suppression velos
        DELETE FROM VELOS
        WHERE VELOS.id_velo = ve.id_velo;
    END LOOP;
    FOR di IN SELECT id_station_1
                FROM ETRE_DISTANTE_DE
                WHERE ETRE_DISTANTE_DE.id_station_1 = OLD.id_station
    LOOP
        -- suppression distances
        DELETE FROM ETRE_DISTANTE_DE
        WHERE ETRE_DISTANTE_DE.id_station_1 = di.id_station_1
        OR ETRE_DISTANTE_DE.id_station_2 = di.id_station_1;
    END LOOP;
    RETURN OLD;
END;    
$trigger_suppr_stations_verif$ LANGUAGE plpgsql; 

CREATE OR REPLACE TRIGGER trigger_suppr_stations_verif
BEFORE DELETE ON STATIONS
FOR EACH ROW
EXECUTE FUNCTION stations_verif();

-- suppresion adherent
CREATE OR REPLACE FUNCTION adherents_verif() RETURNS TRIGGER AS $trigger_suppr_adherents_verif$
DECLARE
    hi RECORD;
BEGIN
    FOR hi IN SELECT id_utilisation
                FROM HISTORIQUE_UTILISATIONS
                WHERE HISTORIQUE_UTILISATIONS.id_adherent = OLD.id_adherent
    LOOP
        -- suppression historiques
        DELETE FROM HISTORIQUE_UTILISATIONS
        WHERE HISTORIQUE_UTILISATIONS.id_utilisation = hi.id_utilisation;
    END LOOP;
    RETURN OLD;
END;    
$trigger_suppr_adherents_verif$ LANGUAGE plpgsql; 

CREATE OR REPLACE TRIGGER trigger_suppr_adherents_verif
BEFORE DELETE ON ADHERENTS
FOR EACH ROW
EXECUTE FUNCTION adherents_verif();

-- suppresion marques
CREATE OR REPLACE FUNCTION marques_verif() RETURNS TRIGGER AS $trigger_suppr_marques_verif$
DECLARE
    ve RECORD;
BEGIN
    FOR ve IN SELECT id_velo
                FROM VELOS
                WHERE VELOS.id_marque = OLD.id_marque
    LOOP
        -- suppression historiques associes
        DELETE FROM HISTORIQUE_UTILISATIONS
        WHERE HISTORIQUE_UTILISATIONS.id_velo = ve.id_velo;
        -- suppression velos
        DELETE FROM VELOS
        WHERE VELOS.id_velo = ve.id_velo;
    END LOOP;
    RETURN OLD;
END;    
$trigger_suppr_marques_verif$ LANGUAGE plpgsql; 

CREATE OR REPLACE TRIGGER trigger_suppr_marques_verif
BEFORE DELETE ON MARQUES
FOR EACH ROW
EXECUTE FUNCTION marques_verif();

-- suppresion velos
CREATE OR REPLACE FUNCTION velos_verif() RETURNS TRIGGER AS $trigger_suppr_velos_verif$
DECLARE
    hi RECORD;
BEGIN
    FOR hi IN SELECT id_utilisation
                FROM HISTORIQUE_UTILISATIONS
                WHERE HISTORIQUE_UTILISATIONS.id_velo = OLD.id_velo
    LOOP
        -- suppression historiques
        DELETE FROM HISTORIQUE_UTILISATIONS
        WHERE HISTORIQUE_UTILISATIONS.id_utilisation = hi.id_utilisation;
    END LOOP;
    RETURN OLD;
END;    
$trigger_suppr_velos_verif$ LANGUAGE plpgsql; 

CREATE OR REPLACE TRIGGER trigger_suppr_velos_verif
BEFORE DELETE ON VELOS
FOR EACH ROW
EXECUTE FUNCTION velos_verif();