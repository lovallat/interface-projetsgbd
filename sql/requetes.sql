/****************************************
        1. CONSULTATION
****************************************/

-- Informations sur les vélos
SELECT VELOS.id_velo, VELOS.reference, VELOS.date_mise_en_service, VELOS.etat, VELOS.niveau_charge_batterie, MARQUES.nom_marque, STATIONS.adresse_station
FROM VELOS
NATURAL JOIN MARQUES
NATURAL JOIN STATIONS;

-- Informations sur les stations
SELECT STATIONS.id_station, STATIONS.adresse_station, STATIONS.nombre_bornes, COMMUNES.nom_ville
FROM STATIONS
NATURAL JOIN COMMUNES;

-- Informations sur les adhérents
SELECT ADHERENTS.id_adherent, ADHERENTS.nom_adherent, ADHERENTS.prenom_adherent, ADHERENTS.adresse_adherent, ADHERENTS.date_adhesion, COMMUNES.nom_ville
FROM ADHERENTS
NATURAL JOIN COMMUNES;

-- Liste des vélos par station
SELECT COALESCE(adresse_station, '- Non disponible') AS adresse_station, 
COMMUNES.nom_ville, VELOS.id_velo, VELOS.reference, 
VELOS.date_mise_en_service, VELOS.etat, 
VELOS.niveau_charge_batterie, MARQUES.nom_marque
FROM VELOS
NATURAL JOIN MARQUES
NATURAL LEFT JOIN STATIONS
NATURAL LEFT JOIN COMMUNES
ORDER BY STATIONS.id_station, STATIONS.adresse_station, VELOS.reference;

-- La liste des vélos en cours d’utilisation
SELECT VELOS.id_velo, VELOS.reference, VELOS.date_mise_en_service, VELOS.etat, VELOS.niveau_charge_batterie, MARQUES.nom_marque
FROM VELOS
NATURAL JOIN MARQUES
WHERE id_station IS NULL;

-- Liste des stations dans une commune donnée
SELECT STATIONS.id_station, STATIONS.adresse_station, STATIONS.nombre_bornes, COMMUNES.nom_ville
FROM STATIONS
NATURAL JOIN COMMUNES
WHERE nom_ville = 'Bordeaux'; -- ici nom_ville est donnee par l'interface

-- Liste des adhérents qui ont emprunté plusieurs au moins deux vélos différents pour un jour donné
SELECT ADHERENTS.*, COMMUNES.nom_ville
FROM ADHERENTS
NATURAL JOIN (SELECT DISTINCT date_debut_utilisation, id_velo, id_adherent FROM HISTORIQUE_UTILISATIONS) AS HISTORIQUE
NATURAL JOIN COMMUNES
WHERE HISTORIQUE.date_debut_utilisation::date = (current_date - 5) -- ici date_debut_utilisation est donnee par l'interface
GROUP BY ADHERENTS.id_adherent, COMMUNES.nom_ville
HAVING COUNT(HISTORIQUE.id_velo) >= 2;

/****************************************
        2. STATISTIQUES
****************************************/

-- Moyenne du nombre d’usagers par vélo par jour
SELECT AVG(avg_jour)
FROM (SELECT CAST(COUNT(*) AS FLOAT) / CAST((SELECT COUNT(*) FROM VELOS) AS FLOAT) as avg_jour
        FROM (SELECT DISTINCT HISTORIQUE_UTILISATIONS.date_debut_utilisation::date, HISTORIQUE_UTILISATIONS.id_adherent 
                FROM HISTORIQUE_UTILISATIONS) AS HISTORIQUE
        GROUP BY HISTORIQUE.date_debut_utilisation::date) AS moyenne_velos_par_jours;

-- Moyenne des distances parcourues par les vélos sur une semaine
SELECT CAST(SUM(ETRE_DISTANTE_DE.distance) AS FLOAT) / 7
FROM (SELECT * 
        FROM HISTORIQUE_UTILISATIONS 
        WHERE DATE_PART('week', HISTORIQUE_UTILISATIONS.date_debut_utilisation::date) = DATE_PART('week', current_date)) AS HISTORIQUE -- ici la date est donnee par l'interface
JOIN ETRE_DISTANTE_DE 
ON (HISTORIQUE .id_station_depart = ETRE_DISTANTE_DE.id_station_1 AND HISTORIQUE .id_station_arrivee = ETRE_DISTANTE_DE.id_station_2);

-- Classement des stations par nombre de places disponibles par commune
SELECT *
FROM (SELECT STATIONS.id_commune, STATIONS.nombre_bornes - COUNT(VELOS.id_velo) AS nbr_places_disponibles 
        FROM STATIONS 
        NATURAL LEFT JOIN VELOS 
        GROUP BY STATIONS.id_station, STATIONS.id_commune) AS STATIONS
ORDER BY STATIONS.id_commune ASC, STATIONS.nbr_places_disponibles ASC;

-- Classement des vélos les plus chargés par station
SELECT VELOS.reference, VELOS.date_mise_en_service, VELOS.etat, VELOS.niveau_charge_batterie, 
MARQUES.nom_marque, STATIONS.adresse_station, COMMUNES.nom_ville
FROM STATIONS 
NATURAL JOIN VELOS
NATURAL JOIN MARQUES
NATURAL JOIN COMMUNES
ORDER BY STATIONS.id_station ASC, VELOS.niveau_charge_batterie DESC;

/****************************************
        3. MISE A JOUR
****************************************/

-- Voici quelques requetes d'exemple pour la modification et la suppression de chaque table (voir sql/data.sql pour les requestes d'ajout)

-- 3.1 MODIFICATION

-- MARQUES : Rachat de la marque Lui Ltd., elle a ete renomme Kais TM.
UPDATE MARQUES 
SET nom_marque = 'Kais TM.' 
WHERE id_marque = 1;

-- COMMUNES : On remplace Pessac par Libourne (il n'y a pas vraiment de sens logique ici)
UPDATE COMMUNES 
SET nom_ville = 'Libourne' 
WHERE id_commune = 1;

-- STATIONS : La station de identifiant 1 a change de localisation apres une renovation
UPDATE STATIONS 
SET adresse_station = '78 rue des Bourgeon' 
WHERE id_station = 1;

-- VELOS : Un des velos a ete pille, on la met donc en reserve pour reparation ou pour en racheter une nouvelle
UPDATE VELOS 
SET etat = 'plus de siège, roue arrière manquante', id_station = NULL 
WHERE id_velo = 2;

-- ADHERENTS : Mme Marie PASCAL s'est mariee et a changer de nom ainsi que changer d'adresse
UPDATE ADHERENTS 
SET nom_adherent = 'DURANT', adresse_adherent = '35 rue des Chaussettes', id_commune = 1 
WHERE id_adherent = 2;

-- HISTORIQUE UTILISATIONS : l'historique en cours d'utilisation a ete terminer
UPDATE HISTORIQUE_UTILISATIONS 
SET date_fin_utilisation = current_timestamp + interval '2 hour' + interval '35 minute', id_station_arrivee = 3 
WHERE id_utilisation = 6;

-- ETRE DISTANTE DE : precision sur une distance ou valeur erroeee
UPDATE ETRE_DISTANTE_DE 
SET distance = 5850 
WHERE id_utilisation = 3;

-- 3.2 SUPPRESSION

-- Scenario : on veut supprimer la commune Pessac, voici toutes les requetes a faire a la main (sans le cascade, et avec le jeu de donnees fourni)
-- c'est juste un cas d'exemple de suppression pour chaque table, on ne ferait pas ca reellement

-- ETRE DISTANTE DE : supprimer les distances avec la seule station a Pessac
DELETE FROM ETRE_DISTANTE_DE
WHERE id_station_1 = 2 OR id_station_2 = 2;

-- HISTORIQUE UTILISATIONS : supprimer les historiques associe a la station a Pessac ou avec l'adherent habitant a Pessac
DELETE FROM HISTORIQUE_UTILISATIONS
WHERE id_velo = 1 OR id_velo = 2 OR id_station_depart = 2 
OR id_station_arrivee = 2 OR id_adherent = 1;

-- ADHERENTS : supprimer les adherents habitant a Pessac
DELETE FROM ADHERENTS
WHERE id_commune = 1;

-- STATIONS : supprimer les stations a Pessac
DELETE FROM STATIONS
WHERE id_commune = 1;

-- MARQUES : supprimer les marques en association avec un velo a Pessac
DELETE FROM MARQUES 
WHERE id_marque = 2;

-- VELOS : supprimer les velos a Pessac
DELETE FROM VELOS
WHERE id_station = 2;

-- COMMUNES : enfin, supprimer la commune Pessac
DELETE FROM COMMUNES 
WHERE id_commune = 1;



