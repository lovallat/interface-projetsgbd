/****************************************
        1. MARQUES
****************************************/

-- Lui Ltd. 
INSERT INTO MARQUES(nom_marque) VALUES ('Lui Ltd.');
-- Symba Co.
INSERT INTO MARQUES(nom_marque) VALUES ('Symba Co.');

/****************************************
        2. COMMUNES
****************************************/

-- Pessac
INSERT INTO COMMUNES(nom_ville) VALUES ('Pessac');
-- Bordeaux
INSERT INTO COMMUNES(nom_ville) VALUES ('Bordeaux');

/****************************************
        3. STATIONS
****************************************/

-- 3 rue du Bouleau (dans Bordeaux) avec 50 bornes
INSERT INTO STATIONS(adresse_station, nombre_bornes, id_commune)
VALUES ('3 rue du Bouleau', 50, (SELECT id_commune FROM COMMUNES WHERE nom_ville = 'Bordeaux'));
-- 2 rue de la Merguez (dans Pessac) avec 25 bornes
INSERT INTO STATIONS(adresse_station, nombre_bornes, id_commune)
VALUES ('2 rue de la Merguez', 25, (SELECT id_commune FROM COMMUNES WHERE nom_ville = 'Pessac'));
-- 15 avenue des Bournes (dans Bordeaux) avec 30 bornes
INSERT INTO STATIONS(adresse_station, nombre_bornes, id_commune)
VALUES ('15 avenue des Bournes', 30, (SELECT id_commune FROM COMMUNES WHERE nom_ville = 'Bordeaux'));

/****************************************
        4. VELOS
****************************************/

-- Le velo QUOKKA-BIKE-001 mise en service il y a 25 jours en etat rayure siege et avec 99% de charge et de marque Symba Co. situe dans la station 2 rue de la Merguez
INSERT INTO VELOS(reference, date_mise_en_service, etat, niveau_charge_batterie, id_marque, id_station)
VALUES ('QUOKKA-BIKE-001', (current_date - 25),
        'rayure siège',
        99,
        (SELECT id_marque FROM MARQUES WHERE nom_marque = 'Symba Co.'),
        (SELECT id_station FROM STATIONS WHERE adresse_station = '2 rue de la Merguez')
        );

-- Le velo GALAKTIC-BIKE-001 mise en service il y a 25 jours en etat correct et avec 10% de charge et de marque Symba Co. situe dans la station 2 rue de la Merguez
INSERT INTO VELOS(reference, date_mise_en_service, etat, niveau_charge_batterie, id_marque, id_station)
VALUES ('GALAKTIC-BIKE-001', (current_date - 25),
        'correct',
        10,
        (SELECT id_marque FROM MARQUES WHERE nom_marque = 'Symba Co.'),
        (SELECT id_station FROM STATIONS WHERE adresse_station = '2 rue de la Merguez')
        );

-- Le velo MONSTER-BIKE-001 mise en service il y a 25 jours en etat bon et avec 85% de charge et de marque Lui Ltd. et ne se situant pas dans une station
INSERT INTO VELOS(reference, date_mise_en_service, etat, niveau_charge_batterie, id_marque, id_station)
VALUES ('MONSTER-BIKE-001', (current_date - 25),
        'bon',
        85,
        (SELECT id_marque FROM MARQUES WHERE nom_marque = 'Lui Ltd.'),
        (SELECT id_station FROM STATIONS WHERE adresse_station = '3 rue du Bouleau') --valeur par defaut, sera mis a NULL apres insertion du dernier historique
        );

-- Le velo QUOKKA-BIKE-002 mise en service il y a 22 jours en etat correct et avec 79% de charge et de marque Symba Co. situe dans la station 3 rue du Bouleau
INSERT INTO VELOS(reference, date_mise_en_service, etat, niveau_charge_batterie, id_marque, id_station)
VALUES ('QUOKKA-BIKE-002', (current_date - 22),
        'neuf',
        79,
        (SELECT id_marque FROM MARQUES WHERE nom_marque = 'Symba Co.'),
        (SELECT id_station FROM STATIONS WHERE adresse_station = '3 rue du Bouleau')
        );

/****************************************
        5. ADHERENTS
****************************************/

-- DUPONT Jean, habitant a 34 rue de la Marne, Bordeaux, inscrite il y a 20 jours
INSERT INTO ADHERENTS(nom_adherent, prenom_adherent, adresse_adherent, date_adhesion, id_commune)
VALUES ('DUPONT', 'Jean', '34 rue de la Marne', (current_date - 20), (SELECT id_commune FROM COMMUNES WHERE nom_ville = 'Pessac'));

-- PASCAL Marie, habitant a 76 avenue du Dr Mince, Bordeaux, inscrit il y a 15 jours
INSERT INTO ADHERENTS(nom_adherent, prenom_adherent, adresse_adherent, date_adhesion, id_commune)
VALUES ('PASCAL', 'Marie', '76 avenue du Dr Mince', (current_date - 15), (SELECT id_commune FROM COMMUNES WHERE nom_ville = 'Bordeaux'));

/****************************************
        6. HISTORIQUE UTILISATIONS
****************************************/

-- Il y a 5 jours de 8h a 12h, le velo QUOKKA-BIKE-001 a ete emprunte de la station 3 rue du Bouleau (Bordeaux) 
-- et est arrive a la station 2 rue de la Merguez (Pessac) par Jean DUPONT
INSERT INTO HISTORIQUE_UTILISATIONS(date_debut_utilisation, date_fin_utilisation, id_velo, id_station_depart, id_station_arrivee, id_adherent)
VALUES ((current_date - 5) + interval '8 hour', 
        (current_date - 5) + interval '12 hour', 
        (SELECT id_velo FROM VELOS where reference = 'QUOKKA-BIKE-001'), 
        (SELECT id_station FROM STATIONS WHERE adresse_station = '3 rue du Bouleau'), 
        (SELECT id_station FROM STATIONS WHERE adresse_station = '2 rue de la Merguez'), 
        (SELECT id_adherent FROM ADHERENTS where nom_adherent = 'DUPONT' and prenom_adherent = 'Jean')
        );

-- Il y a 5 jours de 13h a 16h, le velo GALAKTIC-BIKE-001 a ete emprunte de la station 2 rue de la Merguez (Pessac)
-- et est arrive a la station 3 rue du Bouleau (Bordeaux)  par Jean DUPONT
INSERT INTO HISTORIQUE_UTILISATIONS(date_debut_utilisation, date_fin_utilisation, id_velo, id_station_depart, id_station_arrivee, id_adherent)
VALUES ((current_date - 5) + interval '13 hour', 
        (current_date - 5) + interval '16 hour', 
        (SELECT id_velo FROM VELOS where reference = 'GALAKTIC-BIKE-001'), 
        (SELECT id_station FROM STATIONS WHERE adresse_station = '2 rue de la Merguez'), 
        (SELECT id_station FROM STATIONS WHERE adresse_station = '3 rue du Bouleau'), 
        (SELECT id_adherent FROM ADHERENTS where nom_adherent = 'DUPONT' and prenom_adherent = 'Jean')
        );

-- Il y a 5 jours de 17h a 19h, le velo GALAKTIC-BIKE-001 a ete emprunte de la station 3 rue du Bouleau (Bordeaux) 
-- et est arrive a la station 2 rue de la Merguez (Pessac) par Marie PASCAL
INSERT INTO HISTORIQUE_UTILISATIONS(date_debut_utilisation, date_fin_utilisation, id_velo, id_station_depart, id_station_arrivee, id_adherent)
VALUES ((current_date - 5) + interval '17 hour' + interval '30 minute',
        (current_date - 5) + interval '19 hour',
        (SELECT id_velo FROM VELOS where reference = 'GALAKTIC-BIKE-001'), 
        (SELECT id_station FROM STATIONS WHERE adresse_station = '3 rue du Bouleau'), 
        (SELECT id_station FROM STATIONS WHERE adresse_station = '2 rue de la Merguez'), 
        (SELECT id_adherent FROM ADHERENTS where nom_adherent = 'PASCAL' and prenom_adherent = 'Marie')
        );

-- Il y a deux jours de 13h a 14h, le velo MONSTER-BIKE-001 a ete emprunte de la station 3 rue du Bouleau (Bordeaux) 
-- et est arrive a la station 2 rue de la Merguez (Pessac) par Marie PASCAL
INSERT INTO HISTORIQUE_UTILISATIONS(date_debut_utilisation, date_fin_utilisation, id_velo, id_station_depart, id_station_arrivee, id_adherent)
VALUES ((current_date - 2) + interval '13 hour', 
        (current_date - 2) + interval '14 hour', 
        (SELECT id_velo FROM VELOS where reference = 'MONSTER-BIKE-001'), 
        (SELECT id_station FROM STATIONS WHERE adresse_station = '3 rue du Bouleau'), 
        (SELECT id_station FROM STATIONS WHERE adresse_station = '2 rue de la Merguez'), 
        (SELECT id_adherent FROM ADHERENTS where nom_adherent = 'PASCAL' and prenom_adherent = 'Marie')
        );

-- Il y a deux jours de 18h a 19h, le velo MONSTER-BIKE-001 a ete emprunte de la station 2 rue de la Merguez (Pessac)
-- et est arrive a la station 3 rue du Bouleau (Bordeaux) par Marie PASCAL
INSERT INTO HISTORIQUE_UTILISATIONS(date_debut_utilisation, date_fin_utilisation, id_velo, id_station_depart, id_station_arrivee, id_adherent)
VALUES ((current_date - 2) + interval '18 hour', 
        (current_date - 2) + interval '19 hour', 
        (SELECT id_velo FROM VELOS where reference = 'MONSTER-BIKE-001'), 
        (SELECT id_station FROM STATIONS WHERE adresse_station = '2 rue de la Merguez'), 
        (SELECT id_station FROM STATIONS WHERE adresse_station = '3 rue du Bouleau'), 
        (SELECT id_adherent FROM ADHERENTS where nom_adherent = 'PASCAL' and prenom_adherent = 'Marie')
        );

-- Aujourd'hui a 14h, le velo MONSTER-BIKE-001 a ete emprunte de la station 3 rue du Bouleau (Bordeaux) par Marie PASCAL
INSERT INTO HISTORIQUE_UTILISATIONS(date_debut_utilisation, date_fin_utilisation, id_velo, id_station_depart, id_station_arrivee, id_adherent)
VALUES (current_timestamp, 
        null, 
        (SELECT id_velo FROM VELOS where reference = 'MONSTER-BIKE-001'), 
        (SELECT id_station FROM STATIONS WHERE adresse_station = '3 rue du Bouleau'), 
        NULL, 
        (SELECT id_adherent FROM ADHERENTS where nom_adherent = 'PASCAL' and prenom_adherent = 'Marie')
        );

/****************************************
        7. DISTANCES
****************************************/

-- Distance entre 3 rue du Bouleau (Bordeaux) et 2 rue de la Merguez (Pessac): 5750 metres
INSERT INTO ETRE_DISTANTE_DE(id_station_1, id_station_2, distance)
VALUES ((SELECT id_station FROM STATIONS WHERE adresse_station = '3 rue du Bouleau'),
        (SELECT id_station FROM STATIONS WHERE adresse_station = '2 rue de la Merguez'),
        5750
        );

-- Distance entre 3 rue du Bouleau (Bordeaux) et 15 avenue des Bournes (Bordeaux): 750 metres
INSERT INTO ETRE_DISTANTE_DE(id_station_1, id_station_2, distance)
VALUES ((SELECT id_station FROM STATIONS WHERE adresse_station = '3 rue du Bouleau'),
        (SELECT id_station FROM STATIONS WHERE adresse_station = '15 avenue des Bournes'),
        750
        );

-- Distance entre 2 rue de la Merguez (Pessac) et 15 avenue des Bournes (Bordeaux): 6000 metres
INSERT INTO ETRE_DISTANTE_DE(id_station_1, id_station_2, distance)
VALUES ((SELECT id_station FROM STATIONS WHERE adresse_station = '2 rue de la Merguez'),
        (SELECT id_station FROM STATIONS WHERE adresse_station = '15 avenue des Bournes'),
        6000
        );