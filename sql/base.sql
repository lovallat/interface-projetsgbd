/****************************************
        1. SUPPRESSION TABLES
****************************************/

-- supprimer toutes les tables si elles existent
DROP TABLE IF EXISTS VELOS CASCADE;
DROP TABLE IF EXISTS MARQUES CASCADE;
DROP TABLE IF EXISTS HISTORIQUE_UTILISATIONS CASCADE;
DROP TABLE IF EXISTS STATIONS CASCADE;
DROP TABLE IF EXISTS ETRE_DISTANTE_DE CASCADE;
DROP TABLE IF EXISTS COMMUNES CASCADE;
DROP TABLE IF EXISTS ADHERENTS CASCADE;

/****************************************
        2. CREATION TABLES
****************************************/

CREATE TABLE MARQUES
(
    id_marque  SERIAL PRIMARY KEY,
    nom_marque TEXT NOT NULL
);

CREATE TABLE VELOS
(
    id_velo                SERIAL PRIMARY KEY,
    reference              TEXT     NOT NULL UNIQUE,
    date_mise_en_service   DATE     NOT NULL,
    etat                   TEXT     NOT NULL,
    niveau_charge_batterie SMALLINT NOT NULL,
    id_marque              INTEGER  NOT NULL,
    id_station             INTEGER,
    -- date mise en service <= aujourd'hui
    CONSTRAINT date_mise_en_service_pas_futur 
    CHECK(date_mise_en_service <= current_timestamp),
    CONSTRAINT batterie_entre_0_et_100
    CHECK(niveau_charge_batterie BETWEEN 0 AND 100)
);

CREATE TABLE HISTORIQUE_UTILISATIONS
(
    id_utilisation         SERIAL PRIMARY KEY,
    date_debut_utilisation TIMESTAMP NOT NULL,
    date_fin_utilisation   TIMESTAMP,
    id_velo                INTEGER   NOT NULL,
    id_station_depart      INTEGER   NOT NULL,
    id_station_arrivee     INTEGER,
    id_adherent            INTEGER   NOT NULL,
    -- date debut < date fin
    CONSTRAINT date_fin_sup_date_debut 
    CHECK (date_fin_utilisation > date_debut_utilisation),
    -- jour debut == jour fin
    CONSTRAINT jour_debut_eq_jour_fin 
    CHECK (date_debut_utilisation::date = date_fin_utilisation::date),
    -- date debut <= aujourd'hui
    CONSTRAINT date_debut_pas_futur 
    CHECK(date_debut_utilisation <= current_timestamp),
    -- (date fin == null && station arrivee == null) || (date fin != null && station arrivee != null)
    CONSTRAINT date_fin_et_station_arrivee_nuls_ou_non_nuls
    CHECK ((date_fin_utilisation IS NULL AND id_station_arrivee IS NULL) 
        OR (date_fin_utilisation IS NOT NULL AND id_station_arrivee IS NOT NULL))
);

CREATE TABLE STATIONS
(
    id_station      SERIAL PRIMARY KEY,
    adresse_station TEXT    NOT NULL,
    nombre_bornes   INT     NOT NULL,
    id_commune      INTEGER NOT NULL,
    UNIQUE(adresse_station, id_commune),
    CONSTRAINT nb_bornes_strictement_sup_zero
    CHECK (nombre_bornes > 0)
);

CREATE TABLE ETRE_DISTANTE_DE
(
    distance     INT     NOT NULL,
    id_station_1 INTEGER NOT NULL,
    id_station_2 INTEGER NOT NULL,
    PRIMARY KEY (id_station_1, id_station_2),
    CONSTRAINT distance_strictement_sup_zero
    CHECK (distance > 0)
);

CREATE TABLE COMMUNES
(
    id_commune SERIAL PRIMARY KEY,
    nom_ville  TEXT NOT NULL
);

CREATE TABLE ADHERENTS
(
    id_adherent      SERIAL PRIMARY KEY,
    nom_adherent     TEXT    NOT NULL,
    prenom_adherent  TEXT    NOT NULL,
    adresse_adherent TEXT    NOT NULL,
    date_adhesion    DATE    NOT NULL,
    id_commune       INTEGER NOT NULL,
    CONSTRAINT date_adhesion_pas_futur 
    CHECK(date_adhesion <= current_timestamp) -- date adhesion <= aujourd'hui
);

/****************************************
        3. CLES ETRANGERES
****************************************/

ALTER TABLE VELOS
    ADD CONSTRAINT fk_velo_id_marque
        FOREIGN KEY (id_marque)
            REFERENCES MARQUES (id_marque)
            ON DELETE CASCADE;

ALTER TABLE VELOS
    ADD CONSTRAINT fk_velos_id_station
        FOREIGN KEY (id_station)
            REFERENCES STATIONS (id_station)
            ON DELETE CASCADE;

ALTER TABLE HISTORIQUE_UTILISATIONS
    ADD CONSTRAINT fk_historique_utilisations_id_velo
        FOREIGN KEY (id_velo)
            REFERENCES VELOS (id_velo)
            ON DELETE CASCADE;

ALTER TABLE HISTORIQUE_UTILISATIONS
    ADD CONSTRAINT fk_historique_utilisations_id_station_depart
        FOREIGN KEY (id_station_depart)
            REFERENCES STATIONS (id_station)
            ON DELETE CASCADE;

ALTER TABLE HISTORIQUE_UTILISATIONS
    ADD CONSTRAINT fk_historique_utilisations_id_station_arrivee
        FOREIGN KEY (id_station_arrivee)
            REFERENCES STATIONS (id_station)
            ON DELETE CASCADE;

ALTER TABLE HISTORIQUE_UTILISATIONS
    ADD CONSTRAINT fk_historique_utilisations_id_adherent
        FOREIGN KEY (id_adherent)
            REFERENCES ADHERENTS (id_adherent)
            ON DELETE CASCADE;

ALTER TABLE STATIONS
    ADD CONSTRAINT fk_stations_id_commune
        FOREIGN KEY (id_commune)
            REFERENCES COMMUNES (id_commune)
            ON DELETE CASCADE;

ALTER TABLE ETRE_DISTANTE_DE
    ADD CONSTRAINT fk_etre_distante_de_id_station_1
        FOREIGN KEY (id_station_1)
            REFERENCES STATIONS (id_station)
            ON DELETE CASCADE;

ALTER TABLE ETRE_DISTANTE_DE
    ADD CONSTRAINT fk_etre_distante_de_id_station_2
        FOREIGN KEY (id_station_2)
            REFERENCES STATIONS (id_station)
            ON DELETE CASCADE;

ALTER TABLE ADHERENTS
    ADD CONSTRAINT fk_adherents_id_commune
        FOREIGN KEY (id_commune)
            REFERENCES COMMUNES (id_commune)
            ON DELETE CASCADE;

/****************************************
        4. SELECTIONS
****************************************/

-- vue pour recuperer soit la distance d'une station avec elle-meme ou la distance peut importe l'ordre des identifiants (distance est stockee d'une seule fois)
CREATE OR REPLACE FUNCTION distance_entre(station1 INTEGER, station2 INTEGER)
RETURNS TABLE (distance INTEGER, id_station_1 INTEGER, id_station_2 INTEGER)
LANGUAGE plpgsql
AS $$
BEGIN
    IF station1 = station2 THEN
        RETURN QUERY SELECT 0, station1, station2;
    END IF;
    RETURN QUERY SELECT etre_distante_de.distance, etre_distante_de.id_station_1, etre_distante_de.id_station_2 FROM etre_distante_de WHERE (etre_distante_de.id_station_1 = station1 AND etre_distante_de.id_station_2 = station2) OR (etre_distante_de.id_station_1 = station2 AND etre_distante_de.id_station_2 = station1);
END;$$;


/****************************************
        5. FONCTIONS UTILES
****************************************/

-- verification qu'il restes des places
CREATE OR REPLACE FUNCTION checkIfPlacesLeft(bikeID INTEGER, arrival_station INTEGER, ifAdd BOOLEAN, old_arrival_station INTEGER, startDate TIMESTAMP, updateBikes BOOLEAN)
RETURNS VOID
LANGUAGE plpgsql
AS $$
DECLARE places_restantes INTEGER;
BEGIN
    IF startDate::date = current_date THEN
        SELECT nbr_places_disponibles
        FROM (SELECT STATIONS.id_station, STATIONS.nombre_bornes - COUNT(VELOS.id_velo) AS nbr_places_disponibles 
                FROM STATIONS 
                NATURAL LEFT JOIN VELOS
                GROUP BY STATIONS.id_station, STATIONS.id_commune) AS STATIONS
        WHERE id_station = arrival_station
        INTO places_restantes;
        IF ifAdd = TRUE THEN
            IF places_restantes = 0 THEN
                RAISE EXCEPTION 'Il n&apos;y a plus de places disponibles dans cette station';
            END IF;
        ELSE
            IF places_restantes = 0 AND old_arrival_station != arrival_station THEN
                RAISE EXCEPTION 'Il n&apos;y a plus de places disponibles dans cette station';
            END IF;
        END IF;
        IF updateBikes = TRUE THEN
            UPDATE VELOS SET id_station = arrival_station WHERE id_velo = bikeID;
        END IF;
    END IF;
END;$$;

-- verification que la station originale du velo est egale a la station de debut de l'historique (uniquement pour l'ajout)
CREATE OR REPLACE FUNCTION checkStartingStations(bikeID INTEGER, starting_station INTEGER, startDate TIMESTAMP)
RETURNS VOID
LANGUAGE plpgsql
AS $$
DECLARE old_station RECORD;
BEGIN
    SELECT id_station FROM velos WHERE id_velo = bikeID INTO old_station;
    IF startDate = current_timestamp AND old_station.id_station IS NOT NULL THEN
        IF old_station.id_station != starting_station THEN
            RAISE EXCEPTION 'La station de départ doit être la même que la station choisie';
        END IF;
    END IF;
END;$$;

-- verification que la date_mise_en_service d'un velo est inferieur a la date d'un historique
CREATE OR REPLACE FUNCTION checkPutIntoServiceDate(bikeID INTEGER, startDate TIMESTAMP)
RETURNS VOID
LANGUAGE plpgsql
AS $$
DECLARE put_in_service_date RECORD;
BEGIN
    SELECT date_mise_en_service FROM velos WHERE id_velo = bikeID INTO put_in_service_date;
    IF put_in_service_date.date_mise_en_service > startDate THEN
        RAISE EXCEPTION 'La date de mise en service doit être inférieur à la date d&apos;utilisation';
    END IF;
END;$$;

-- verification que la date d'adhesion d'un adherent est inferieur a la date d utilisation
CREATE OR REPLACE FUNCTION checkAdmissionDate(memberID INTEGER, startDate TIMESTAMP)
RETURNS VOID
LANGUAGE plpgsql
AS $$
DECLARE admission_date RECORD;
BEGIN
    SELECT date_adhesion FROM adherents WHERE id_adherent = memberID INTO admission_date;
    IF admission_date.date_adhesion > startDate THEN
        RAISE EXCEPTION 'La date d&apos;adhesion doit être inférieur à la date d&apos;utilisation';
    END IF;
END;$$;

-- verification que si la batterie est en dessous ou egale a 10%, alors on ne peut pas prendre le velo (uniquement pour l'ajout)
CREATE OR REPLACE FUNCTION checkBikeBattery(bikeID INTEGER, usageDate DATE)
RETURNS VOID
LANGUAGE plpgsql
AS $$
DECLARE bike_battery RECORD;
BEGIN
    SELECT niveau_charge_batterie FROM velos WHERE id_velo = bikeID INTO bike_battery;
    IF usageDate = current_date AND bike_battery.niveau_charge_batterie <= 10 THEN
        RAISE EXCEPTION 'Le vélo doit avoir au moins plus de 10&percnt; de charge';
    END IF;
END;$$;

-- verification que les dates d'un historique ne coincide pas avec un historique deja existant
CREATE OR REPLACE FUNCTION checkOverlappingDates(bikeID INTEGER, startDate TIMESTAMP, endDate TIMESTAMP, historyID INTEGER)
RETURNS VOID
LANGUAGE plpgsql
AS $$
DECLARE t_row RECORD;
BEGIN
    FOR t_row in (SELECT id_utilisation, id_velo, date_debut_utilisation, date_fin_utilisation FROM historique_utilisations WHERE id_velo = bikeID) LOOP
        IF historyID != t_row.id_utilisation AND (SELECT (t_row.date_debut_utilisation, COALESCE(t_row.date_fin_utilisation, current_timestamp + interval '1 hour')) OVERLAPS (startDate, COALESCE(endDate, current_timestamp + interval '1 hour'))) THEN
            RAISE EXCEPTION 'Les dates ne doivent pas coincider avec un historique déjà enregistré (conflit avec l&apos;historique numéro %)', t_row.id_utilisation;
        END IF;
    END LOOP;
END;$$;

-- verification si la distance entre deux stations existe deja
CREATE OR REPLACE FUNCTION checkIfDistanceExists(station1 INTEGER, station2 INTEGER, oldStation1 INTEGER, oldStation2 INTEGER)
RETURNS VOID
LANGUAGE plpgsql
AS $$
DECLARE distance_existante RECORD;
BEGIN
    SELECT distance FROM distance_entre(station1, station2) INTO distance_existante;
    raise notice '1: %', oldStation1;
    raise notice '2: %', oldStation2;
    IF (oldStation1 IS NULL AND oldStation2 IS NULL) OR (station1 != oldStation1 OR station2 != oldStation2) THEN
        IF distance_existante IS NOT NULL THEN
            RAISE EXCEPTION 'La distance entre les stations numéro % et % existe déjà (la distance reciproque et la distance d&apos;une station avec elle même n&apos;a pas besoin d&apos;être stockée)', station1, station2;
        END IF;
    END IF;
END;$$;

/****************************************
        6. TRIGGERS
****************************************/

-- 6.1 TRIGGERS SUR LA CREATION

-- verification unique a l'insertion d'un historique
CREATE OR REPLACE FUNCTION insert_historique_verifs() RETURNS TRIGGER AS $trigger_insert_historique_verifs$
BEGIN
    -- verification que la station originale du velo est egale a la station de debut de l'historique
    PERFORM checkStartingStations(NEW.id_velo, NEW.id_station_depart, NEW.date_debut_utilisation);
    -- verification du niveau de charge de la batterie du velo
    PERFORM checkBikeBattery(NEW.id_velo, NEW.date_debut_utilisation::date);
    -- verification qu'il restes des places
    PERFORM checkIfPlacesLeft(NEW.id_velo, NEW.id_station_arrivee, TRUE, NULL, NEW.date_debut_utilisation, TRUE);
    RETURN NEW;
END;    
$trigger_insert_historique_verifs$ LANGUAGE plpgsql; 

CREATE OR REPLACE TRIGGER trigger_insert_historique_verifs
BEFORE INSERT ON HISTORIQUE_UTILISATIONS
FOR EACH ROW
EXECUTE FUNCTION insert_historique_verifs();

-- verification qu'il y a des places dans la station (insertion d'un velo)
CREATE OR REPLACE FUNCTION insert_velos_verif_place() RETURNS TRIGGER AS $trigger_insert_velos_verif_place$
BEGIN
    PERFORM checkIfPlacesLeft(NEW.id_velo, NEW.id_station, TRUE, NULL, localtimestamp, FALSE);
    RETURN NEW;
END;    
$trigger_insert_velos_verif_place$ LANGUAGE plpgsql;

CREATE OR REPLACE TRIGGER trigger_insert_velos_verif_place 
BEFORE INSERT ON VELOS
FOR EACH ROW
EXECUTE FUNCTION insert_velos_verif_place();

-- 6.2 MISE A JOUR

-- verification unique a la maj d'un historique
CREATE OR REPLACE FUNCTION maj_historique_verifs() RETURNS TRIGGER AS $trigger_maj_historique_verifs$
BEGIN
    PERFORM checkIfPlacesLeft(NEW.id_velo, NEW.id_station_arrivee, FALSE, OLD.id_station_arrivee, NEW.date_debut_utilisation, TRUE);
    RETURN NEW;
END;    
$trigger_maj_historique_verifs$ LANGUAGE plpgsql; 

CREATE OR REPLACE TRIGGER trigger_maj_historique_verifs
BEFORE UPDATE ON HISTORIQUE_UTILISATIONS
FOR EACH ROW
EXECUTE FUNCTION maj_historique_verifs();

-- verification qu'il y a des places dans la station (mise a jour)
CREATE OR REPLACE FUNCTION maj_velos_verif_place() RETURNS TRIGGER AS $trigger_maj_velos_verif_place$
DECLARE old_station RECORD;
BEGIN
    SELECT id_station FROM velos WHERE id_velo = NEW.id_velo INTO old_station;
    PERFORM checkIfPlacesLeft(NEW.id_velo, NEW.id_station, FALSE, old_station.id_station, localtimestamp, FALSE);
    RETURN NEW;
END;    
$trigger_maj_velos_verif_place$ LANGUAGE plpgsql;

CREATE OR REPLACE TRIGGER trigger_maj_velos_verif_place 
BEFORE UPDATE ON VELOS
FOR EACH ROW
EXECUTE FUNCTION maj_velos_verif_place();

-- 6.3 INSERTION & MISE A JOUR

-- mettre le nom de famille en majuscules d'un adherent avant maj ou insertion (convention que nous avons decidé)
CREATE OR REPLACE FUNCTION adherent_nom() RETURNS TRIGGER AS $trigger_insert_maj_insert_adherent_nom$
BEGIN
    NEW.nom_adherent = UPPER(NEW.nom_adherent);
    RETURN NEW;
END;    
$trigger_insert_maj_insert_adherent_nom$ LANGUAGE plpgsql; 

CREATE OR REPLACE TRIGGER trigger_insert_maj_insert_adherent_nom
BEFORE INSERT OR UPDATE ON ADHERENTS
FOR EACH ROW
EXECUTE FUNCTION adherent_nom();

-- verifications communs a l'ajout et a la modification
CREATE OR REPLACE FUNCTION historique_utilisations_verifs_commun() RETURNS TRIGGER AS $trigger_insert_update_historique_utilisations_verifs_commun$
BEGIN
    -- verification que la date de mise en service est inferieur a la date d'utilisation
    PERFORM checkPutIntoServiceDate(NEW.id_velo, NEW.date_debut_utilisation);
    -- verification que la date d'adhesion est inferieur a la date d'utilisation
    PERFORM checkAdmissionDate(NEW.id_adherent, NEW.date_debut_utilisation);
    -- verification d'historique qui coincide avec une autre
    PERFORM checkOverlappingDates(NEW.id_velo, NEW.date_debut_utilisation, NEW.date_fin_utilisation, NEW.id_utilisation);
    RETURN NEW;
END;   
$trigger_insert_update_historique_utilisations_verifs_commun$ LANGUAGE plpgsql;

CREATE OR REPLACE TRIGGER trigger_insert_update_historique_utilisations_verifs_commun
BEFORE INSERT OR UPDATE ON HISTORIQUE_UTILISATIONS
FOR EACH ROW
EXECUTE FUNCTION historique_utilisations_verifs_commun();

-- verifications que la distance entre deux stations n'existe pas deja
CREATE OR REPLACE FUNCTION distances_verif_existance() RETURNS TRIGGER AS $trigger_insert_update_distances_verif_existance$
BEGIN
    PERFORM checkIfDistanceExists(NEW.id_station_1, NEW.id_station_2, OLD.id_station_1, OLD.id_station_2);
    RETURN NEW;
END;   
$trigger_insert_update_distances_verif_existance$ LANGUAGE plpgsql;

CREATE OR REPLACE TRIGGER trigger_insert_update_distances_verif_existance
BEFORE INSERT OR UPDATE ON ETRE_DISTANTE_DE
FOR EACH ROW
EXECUTE FUNCTION distances_verif_existance();