# Interface Projet SGBD

> Code source disponible sur le dépôt [git](https://gitlab.com/lovallat/interface-projetsgbd).

> Une instance de l'interface présente dans ce dépôt se trouve [ici](https://web.projetsgbd.louis-vallat.xyz).

## Structure des fichiers sources

Ce repository se découpe de la façon suivante :

- sql/ Les requêtes SQL du projet.
- src/ Le code source (PHP 8) de l'interface.
    - assets/php/ Le code source commun aux différentes pages (connexion, header...)
- .gitlab-ci.yml Le fichier de configuration pour le pipeline d'intégration continue.
- Dockerfile Le fichier de configuration pour construire ce projet sous forme d'image Docker.

## Instructions d'installation

Pour installer ce projet, il vous faut en prérequis :

- un serveur PHP (version >= 8) avec la prise en charge pour les fonctions `pgsql` (`pg_query` par exemple).
- un serveur PostgreSQL configuré et un compte administrateur ayant la capacité de créer des bases de données sur ce même serveur.
- le programme `psql` qui permet de se connecter au Système de Gestion de Base de Données.

Pour mettre en place la base de données et ses tables associées, la commande à
utiliser est la suivante :

```bash
psql -U <username > -d <db> -h <host > -p <port number > -f sql/setup.sql -f sql/base.sql
```

Ensuite, pour lancer un serveur PHP (version >= 8) localement, vous pouvez lancer
la commande suivante dans un terminal, en vous trouvant dans le dossier `src/`
de ce dépôt :

```bash
DB_USERNAME=user DB_PASSWORD=pass DB_PORT=5432 DB_HOST=host DB_NAME=nomdb php -S localhost :8000
```


## Auteurs

- Sylvain Raïs
- Kaitlin Rooke
- Louis Vallat

